##
## set environment
##

export TEXINPUTS = .:./styles/:./bibtex/:
export BSTINPUTS = .:./styles/:./bibtex/:
export BIBINPUTS = .:./styles/:./bibtex/:

##
## macrodefinitions
##

LATEX  = pdflatex
BIBTEX = bibtex
MAKEGLOSSARIES = makeglossaries
SRC    = main

REMOVE = *.tex~ *.aux *.glo *.gls *.cb *.dvi *.log *.toc *.lof *.bbl *.blg *.ilg *.cb? *.idx *.out *.synctex.gz *.nlo *.nls *.bak *.acn *.acr *.ist *.alg *.glsdefs *.fls *.fdb_latexmk *.lol *.glg *.glo *.gls *.run.xml *.bcf *-blx.bib *.lot *-write.tex *.mw

##
## rules
##

all: build clean

build:
	$(LATEX) -synctex=1 -shell-escape -interaction=nonstopmode $(SRC)
	$(MAKEGLOSSARIES) $(SRC)
	$(LATEX) -synctex=1 -shell-escape -interaction=nonstopmode $(SRC)
	$(BIBTEX) $(SRC)
	$(LATEX) -synctex=1 -shell-escape -interaction=nonstopmode $(SRC)
	$(LATEX) -synctex=1 -shell-escape -interaction=nonstopmode $(SRC)

clean:
	$(foreach extension,$(REMOVE),$(shell find . \( -path "./.git/*" \) -o -name '$(extension)' -type f -delete))
	rm -rf _minted-main/
