# Finaler Stand der Arbeit:  
[Datenschutzfreundliche, blinde Signaturverfahren auf Basis von elliptischen Kurven zur Integration in eingebettete Systeme] (https://gitlab.com/0ndo/master_thesis_blind_signature/raw/master/main.pdf)

# Abstract 
Das Konzept der digitalen Signatur, als modernes Äquivalent zur klassischen  
Unterschrift, ist seit seiner Einführung im Rahmen der Public-Key-Kryptographie  
zu einem elementaren Bestandteil in den Bemühungen geworden, die elektronische  
Kommunikation sicherer zu gestalten. Unter ihren verschiedenen Ausprägungsarten,  
welche sich seit den Anfängen entwickelt haben, nehmen die blinden Signaturverfahren,  
mit ihrem speziellen datenschutzfreundlichen Grundkonzept, eine besondereStellung  
ein. Diese Stellung ergibt sich durch die gebotene Kombination von Sicherheit  
und Privatsphäre. Hierbei werden zwei Aspekte der öffentlichen Kommunikation  
miteinander verbunden, welche sonst oft als widersprüchlich zueinander angesehen  
werden. So können die blinden Signaturverfahren, durch ihre grundlegende Anonymität  
und die weiterhin gegebenen Eigenschaften zur sicheren Verifizierung des  
Signaturausstellers, auch für kritischste Anwendungsmebiete wie digitale Währungen  
und elektronisches Geld verwendet werden.  
  
Um ihre Verbreitung weiter voranzutreiben, behandelt diese Arbeit die Adaption  
blinder Signaturverfahren in den aktuellen Entwicklungen der Public-Key-Kryptographie  
und im speziellen der Elliptischen-Kurven-Kryptographie. Dabei betrachtet sie  
aktuelle Verfahren auf Basis elliptischer Kurven und beschreibt eine mögliche  
Herangehensweise bei der Integration in den De-facto-Standard OpenPGP. Als Basis  
für diese Ausführungen dient die prototypische Entwicklung einer Webapplikation  
zur Pseudonymvergabe unter Verwendung blinder Signaturverfahren auf Basis von  
elliptischer Kurven.  

# Wo findet sich die Software?
Die im Rahmen der Arbeit entwickelte Software findet sich im nachstehenden Repository: https://gitlab.com/0ndo/verify_me

# Build

Um die Pdf zu erstellen wird zusätzlich das Paket 'python-pygments' benötigt.
