%
% Beuth Hochschule für Technik --  Abschlussarbeit
%
% Motivation und Zielsetzung
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Blinde Signaturverfahren}
\label{ch:Motivation}
Dieses einführende Kapitel dient der Einordnung der vorliegenden Arbeit in den aktuellen Forschungsstand und gibt einen Überblick über die behandelte Thematik. Im anschließenden \cref{sec:Motivation - Motivation} (\nameref{sec:Motivation - Motivation}) werden die zu Grunde liegenden Ideen beschrieben. Die \cref{sec:Motivation - Zielsetzung,sec:Motivation - Abgrenzung} (\nameref{sec:Motivation - Zielsetzung} und \nameref{sec:Motivation - Abgrenzung}) geben einen klaren Überblick über die Schwerpunkte und abschließend wird in \cref{sec:Motivation - Aufbau} die Struktur der restlichen Arbeit beschrieben.

\section{Motivation}
\label{sec:Motivation - Motivation}
Blinde Signaturverfahren finden vor allem auf Grund ihres datenschutzfreundlichen Grundkonzepts in vielen Einsatzgebieten Anwendung, in denen die ihnen zugrunde liegende Anonymität gefragt ist, so zum Beispiel elektronische Währungen und anonyme digitale Wahlen. (vgl. \autocite{Chaum1985Security, Schneier1996Applied, Ermer2015Datenschutz}). Während die Adaption der \acrlong{ECC}, d.h. der \acrlong{PKC} auf Basis von elliptischen Kurven, in diesen Bereichen in den Jahrzehnten seit ihrer konzeptionellen Einführung stark voran geschritten ist \autocite{GnuPG2014Modern, Bos2014Elliptic}, ist die Auswahl an blinden Signaturverfahren, die auf der \gls{ECC} basieren, immer noch vergleichsweise gering (vgl. \cref{sec:Konzeption - Signaturverfahren}).

Parallel hat sich, zusätzlich zu den bereits angesprochenen klassischen Anwendungsbereichen der blinden Signaturen, in den letzten Jahren durch die stetige Entwicklung der Heimautomatisierung und speziell den intelligenten Messsystemen ein neues potentielles Anwendungsgebiet entwickelt in dem bisher große öffentliche Datenschutzbedenken vorherrschen (siehe \autocite{FAZ2015Moderne, Zeit2015Stromkunden, Datenschutz2012Orientierungshilfe}). Da die entsprechenden technischen Richtlinien \autocite{BSI-TR-03109-3} des aktuellen Gesetzesentwurfs zur \glqq Digitalisierung der Energiewende\grqq{} \autocite{BMWi2015Digitalisierung} in diesem Bereich eine strikte Verwendung von standardisierter, \gls{ECC}-basierter Kryptographie vorsieht, wäre auch in diesem Feld eine Umsetzung von blinden Signaturverfahren auf Basis von elliptischen Kurven denkbar.

Um die Verbreitung entsprechender Verfahren voranzutreiben müsste also gewährleistet sein, dass diese nicht nur existieren und sicher sind, sie müssten auch in aktuellen Standards formalisiert sein, um aktuellen oder zukünftigen Regulierungen zu entsprechen. Da sie zum aktuellen Zeitpunkt jedoch nicht in Standards wie das \gls{OpenPGP}-Format und entsprechend auch nicht in dessen Implementierungen integriert sind, wird im Rahmen dieser Arbeit, neben der reinen Existenz- und Sicherheitsüberprüfung (\cref{sec:Konzeption - Signaturverfahren}), auch aufgezeigt, wie eine Einbettung in aktuelle Standards erreicht werden kann und welche Anpassungen dafür notwendig sind. (\cref{sec:Umsetzung - Integration}).

\clearpage

\section{Zielsetzung}
\label{sec:Motivation - Zielsetzung}

Aus der, im obigen Abschnitt beschriebenen, Motivation leiten sich die folgenden zentralen Fragestellungen dieser Arbeit ab:

\begin{itemize}

   \item Wie ist der aktuelle Stand der Veröffentlichungen im Bereich der elliptischen Kurven basierten blinden Signaturverfahren und ist dieser soweit fortgeschritten, dass eine Implementierung in aktuelle Softwareprojekte erfolgen kann, ohne das weitergehende mathematische Betrachtungen notwendig werden?

   \item Welche Algorithmen stehen für eine solche Integration zur Verfügung und in welchen Punkten unterscheiden sie sich?
  
   \item Auf Basis welcher Technologien kann eine möglichst einfache, robuste Integration sowohl in neue, als auch in bestehende Projekte erfolgen?
  
  \item Sind blinde Signaturverfahren bereits in die aktuellen Standards integriert oder sind Anpassungen notwendig, um die Integration zu ermöglichen? Falls eventuelle Anpassungen notwendig sein sollten, wie sehen diese aus und wie umfangreich sind sie?
  
\end{itemize}

Basierend auf diesen Fragestellungen wird der Entwurf eines Prototyps zur Verteilung von blind erzeugten Schlüsselsignaturen mittels entsprechender Signaturverfahren auf der Basis von elliptischen Kurven als Ziel definiert. Wichtig ist dabei, dass eine Verifizierung der blinden digitalen Signaturen unter Verwendung der im \cref{sec:Stand der Technik - OpenPGP-Standard} (\nameref{sec:Stand der Technik - OpenPGP-Standard}) besprochenen Standards sichergestellt wird, da nur bei entsprechender Standardkonformität die zukünftige Verbreitung gewährleistet werden kann. Sollten die Implementierung der Signaturverfahren nicht durch die aktuellen Standards abgedeckt sein, muss darüber hinaus ein Weg aufgezeigt werden, wie die Standards mittels minimaler Anpassungen so erweitert werden können, dass eine Verwendung elliptischer Kurven basierter blinden Signaturverfahren realisierbar ist.

\section{Abgrenzung}
\label{sec:Motivation - Abgrenzung}

Um dem begrenzten Rahmen der vorliegenden Arbeit gerecht zu werden und die Arbeit auf die wesentliche Aspekte zu fokussieren, besteht die Notwendigkeit einer klaren Abgrenzung der im vorhergehenden Abschnitt beschriebenen Ziele. Die folgenden theoretischen Betrachtungen und Implementierungsdetails sind deshalb \emph{nicht} Bestandteil dieser Arbeit:

\paragraph*{Theoretische Betrachtungen}
\begin{itemize}
   \item Bei der Beschreibung der mathematischen Grundlagen erfolgt eine Beschränkung auf jene, welche für das Verständnis der Konzeption und Implementierung des Prototyps notwendig sind. Für den Fall, dass eine detailliertere Betrachtung notwendig oder gewünscht ist, wird auf die entsprechende Grundlagenliteratur verwiesen.
  
   \item Aufgrund von fehlenden Veröffentlichungen zu \gls{EdDSA} kompatiblen blinden Signaturverfahren wird auf eine Aufarbeitung der theoretischen Grundlagen verzichtet.

\end{itemize}

\paragraph*{Implementierung}
\begin{itemize}
  \item Es erfolgt ausschließlich eine beispielhafte, prototypische Umsetzung einiger ausgewählter Algorithmen zur Erzeugung von blinden Signaturen, ohne den Anspruch der Implementierung einer erschöpfenden Liste aller veröffentlichten Algorithmen.
  
   \item Darüber hinaus ist es nicht das Ziel dieser Arbeit einen eigenen Algorithmus zur Erzeugung von blinden Signaturen zu entwerfen oder zu implementieren. Die Umsetzung bezieht sich ausschließlich auf bereits veröffentlichte Signaturverfahren. Dies erfolgt in der Annahme, dass sich die Entwickler der Verfahren ihrer Verantwortung bei der Veröffentlichung der Algorithmen bewusst gewesen sind und entsprechend ihrer Erfahrung sorgfältige Sicherheitsbetrachtungen durchgeführt wurden.
  
   \item Der resultierende Prototyp und seine Bestandteile sollte nicht im Produktiveinsatz verwendet werden ohne vorher die Umsetzung einer erneuten Sicherheitsbetrachtung zu unterziehen.
\end{itemize}

\section{Aufbau}
\label{sec:Motivation - Aufbau}

Im Folgenden wird ein Überblick über den Aufbau der Arbeit gegeben, welche aus drei Teilen besteht.

Der erste Teil (\cref{ch:Theoretische Grundlagen,ch:Stand der Technik}) betrachtet die Grundlagen dieser Arbeit. In ihm werden mathematische Voraussetzungen, wie die Modulare Arithmetik und Elliptische Kurven sowie kryptographische Grundlagen, wie der \gls{ECDSA} und die Theorie der blinden Signaturen, erläutert. Darüber hinaus erfolgt eine Betrachtung des aktuellen Stands der Technik auf dem Gebiet der blinden Signaturverfahren auf Basis von elliptischen Kurven und der allgemeinen Integration der elliptischen Kurven. Im zweiten Teil der Arbeit, bestehend aus den Kapiteln \ref{ch:Konzeption} und \ref{ch:Umsetzung}, wird die grundlegende Problemstellung der Integration von blinden Signaturverfahren und die notwendigen Anforderungen an eine Testimplementierung erarbeitet. Anschließend wird die gewählte Herangehensweise sowie die verwendeten Technologien erläutert und es erfolgt eine Analyse der Umsetzung, inklusive der im Verlauf aufgetretenen Hürden und Hindernisse. Abschließend wird im \cref{ch:Auswertung} eine Auswertung der zuvor aufgeworfenen Problemstellungen und eine kritische Betrachtung der Implementierungsergebnisse vorgenommen. Außerdem wird ein kurzer Ausblick auf mögliche weitere Entwicklungen im Bereich der blinden Signaturverfahren gegeben.