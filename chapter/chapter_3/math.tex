%
% Beuth Hochschule für Technik --  Abschlussarbeit
%
% Mathematische Grundlagen 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Mathematische Grundlagen}
Seit ihrer Formalisierung durch Elwood Shannon im Jahre 1949 \autocite{Shannon1949Communication}, ist die Mathematik eine wichtige Grundlage der modernen Kryptographie. Dieser Abschnitt liefert eine Einführung in die für diese Arbeit wichtigsten Themengebiete, wie Modulare Arithmetik, Elliptische-Kurven-Kryptographie und das diskrete Logarithmus Problem in elliptischen Kurven. Ergänzungen zu weiterführenden Themen finden sich in \cref{sec:Theoretische Grundlagen - Vertiefung} - \nameref{sec:Theoretische Grundlagen - Vertiefung}.

\subsection{Modulare Arithmetik}
Die modulare Arithmetik ist ein Teilgebiet der Mathematik, spezieller der Zahlentheorie, welches erstmals in den Ausarbeitungen von \textcite{Gauss1889Arithmetik} und seinen Betrachtungen zur höheren Arithmetik formal zusammengefasst wurde. Während sich die Zahlentheorie im Allgemeinen mit den Mengen der ganzen und natürlichen Zahlen ($\mathbb{Z}$ und $\mathbb{N}$) ihren Eigenschaften und Besonderheiten beschäftigt \autocite[S. 2]{Bundschuh2008Einfuehrung}, steht in der modularen Arithmetik der Begriff der Kongruenz (vgl. Unterabschnitt \nameref{subsubsec:Modulare Arithmetik - Kongruenz}) und die ihr zu Grunde liegende Division mit Rest sowie das modulare Rechnen im Mittelpunkt \autocite[S. 78]{Bundschuh2008Einfuehrung}. Sie ist spätestens seit der Einführung von \gls{PKC}-Verfahren wie dem \gls{RSA} oder der \gls{ECC} ein fundamentaler Eckpfeiler der modernen Kryptographie (vgl. u. a. \autocite{Shannon1949Communication, Koblitz1987Elliptic, Schneier1996Applied}).

\subsubsection{Division mit Rest und Modulares Rechnen}
Da bei der Division zweier ganzer oder natürlicher Zahlen $\frac{n}{m}$ mit $n,m \in \mathbb{Z}$ oder $n,m \in \mathbb{N}$ nicht auf eine Darstellung in Form von Brüchen zurückgegriffen werden kann, wird in der Arithmetik die Division mit Rest verwendet. Diese Art der Division ist, entsprechend den Ausführungen in \textcite[S.16]{Bundschuh2008Einfuehrung} und der daraus entnommenen Definition, wie folgt beschrieben:

\begin{definition} (\emph{Divisionsalgorithmus.}):
\label{def:Division}
Zu jedem Paar $(n, m)$ ganzer Zahlen mit $m > 0$ existiert genau ein Paar $(a, b)$ ganzer Zahlen, so daß [sic] gilt
%
   \begin{equation} \label{eq:Division}
      n = am + b \text{    und    } 0 \leq b < m.
   \end{equation}
\end{definition}
%
Hierbei wird $a$ als der \emph{Quotient} der Division bezeichnet und $b$ als der zuvor angesprochene \emph{Rest}. Ist man ausschließlich am resultierenden Rest $b$ interessiert, wird dies verdeutlicht durch die Verwendung der als \emph{Modulo} bezeichneten Rechenoperation, welche dem oben angesprochenen Zahlenpaar $(n, m)$ ausschließlich den eindeutigen Rest $b$ zuweist und den Quotienten $a$ verwirft\footnote{Ein Beispiel für die \cref{def:Division}:  Für die Division von ${n_1 = 100}$ durch ${m = 3}$ existiert genau ein Paar ${(a, b)}$ mit ${a = 33}$ und ${b = 1}$ für das gilt ${100 = 33 * 3 + 1}$, woraus folgt ${\frac{100}{3} = 33 \text{ Rest } 1}$.}.

Ein einfaches Beispiel für die Verwendung der Division mit Rest ist die Darstellung der Uhrzeit. Gerade am Beispiel einer alten Analoguhr - wie in \cref{fig:Uhr_Modulo} - wird bereits Kindern das Rechnen mit Rest näher gebracht. Die Uhr ist hierbei eine Verbildlichung der ganzzahligen Division durch 12, wobei die aktuelle Uhrzeit dem Rest der Division entspricht. Addiert man also, wie in der \cref{fig:Uhr_Modulo} dargestellt, auf 9 Uhr 4 weitere Stunden, so zeigt der Zeiger wiederum auf die 1, da $ {\frac{13}{12} = 1 \text{ Rest } 1}$.

Das Rechnen auf Basis des Rests wird auch als modulares Rechnen bezeichnet, hierbei wird anstelle der Division der sogenannte Modulo-Operator ($\bmod$) verwendet, welcher im Rahmen des nachstehenden Abschnitts zur Kongruenz eingeführt wird. In der Informatik wird für diesen Operator häufig auch das $\%$-Symbol verwendet.

\begin{figure}[!t]
   \begin{minipage}[c]{0.5\textwidth}
      \includegraphics[keepaspectratio, width=\textwidth]{WC2007Clock.png}
   \end{minipage}\hfill
   \begin{minipage}[c]{0.45\textwidth}
      \mycaption{Modulo am Beispiel Uhr}{Die Uhr bietet ein einfaches Beispiel, um das Rechnen mit Rest und die Idee des Modulo-Operators bereits Kindern zu erläutern, denn obwohl auf ihr ausschliesslich die Zahlen 1 - 12 (oder hier 0 - 11) dargestellt sind, lässt sich jede Zeitspanne, egal ob in der Vergangenheit oder Zukunft, in eine neue Uhrzeit umrechnen (Bildquelle: \autocite{WC2007Clock}).}
      \label{fig:Uhr_Modulo}
   \end{minipage}
\end{figure}

\enlargethispage{-\baselineskip}

\subsubsection{Kongruenz}
\label{subsubsec:Modulare Arithmetik - Kongruenz}
Der Begriff Kongruenz beschreibt in der Zahlentheorie das Verhältnis der Differenz zweier ganzer Zahlen ${n_1 - n_2}$ mit ${n_1, n_2 \in \mathbb{Z}}$ in Bezug auf ihre Teilbarkeit durch ${m\ (m \in \mathbb{Z})}$. Er wurde von \textcite[Abschn. 1, Art. 1]{Gauss1889Arithmetik} durch die folgende Formalisierung festgehalten:
%
\begin{quote}
   Wenn die Zahl $m$ in der Differenz der Zahlen $n_1, n_2$ aufgeht, so werden $n_1$ und $n_2$ nach $m$ \textbf{congruent} [sic!], im anderen Falle \textbf{incongruent} [sic!] genannt. Die Zahl $m$ nennen wir den \textbf{Modul}. Jede der beiden Zahlen $n_1, n_2$ heißt im ersteren Falle \textbf{Rest}, im letzteren Falle aber \textbf{Nichtrest} der anderen. [...]\footnote{Die Bezeichnung der Variablen wurde an das Beispiel angepasst.}
\end{quote}

Bezieht man Gaußs Formalisierung auf die im vorhergehenden Abschnitt eingeführte Gleichung zur Division mit Rest, bedeutet dies vereinfacht ausgedrückt, dass die Zahlen ${n_1, n_2}$ als kongruent zu $m$ bezeichnet werden, wenn die Division $\frac{n_1}{m}$ und die Division $\frac{n_2}{m}$ im selben Rest resultieren. Dies geschieht, da es sich bei der Differenz der beiden Zahlen ${n_1, n_2}$ um ein Vielfaches des Divisors $m$ handelt. Ergänzt wird diese Beschreibung, durch die Definition \ref{def:Kongruenz}, welche aus \autocite[S.79, Def. 1]{Bundschuh2008Einfuehrung} entnommen wurde:
%
\begin{definition} (\emph{Definition der Kongruenz, elementare Eigenschaften.})
\label{def:Kongruenz}
Seien $m \neq 0, n_1, n_2$ ganze Zahlen. Man nennt $n_1$ kongruent (zu) $n_2$ modulo $m$ genau dann, wenn $m|(n_1 - n_2)$ gilt; man schreibt dies als
%
   \begin{equation}
      n_1 \equiv n_2 \pmod{m}
   \end{equation}
   \vspace{2cm}
\end{definition}
%
Unter Verwendung der Division mit Rest und der eben eingeführten Schreibweise kann der Begriff Kongruenz mathematisch auch wie folgt beschrieben werden:
%
\begin{equation}
   \left.\begin{aligned}
      n_1 &= a_1m + b_1 \qquad (n_1 \bmod{m}),\\
      n_2 &= a_2m + b_2 \qquad (n_2 \bmod{m}),\\
      n1 &\equiv n_2 \text{ wenn } b_1 = b_2 \text{ für } a_i, b_i, n_i, m \in \mathbb{Z} \text{ gilt}.
   \end{aligned}\qquad\right\rbrace
\end{equation}
%
Ergänzt wird die Definition \ref{def:Kongruenz} durch die ebenfalls aus \autocite[S. 79]{Bundschuh2008Einfuehrung} entnommenen nachstehenden Anmerkungen mit Bezug auf die Sätze zur Teilbarkeit ganzer Zahlen. Die zu Grunde liegenden Sätze finden sich unter anderem in \autocite[S.4]{Bundschuh2008Einfuehrung}.
%
\begin{anmerkung} (\emph{zur Definition \ref{def:Kongruenz}}): 
\label{anm:Kongruenz}
Aus Definition \ref{def:Kongruenz} folgt insbesondere
   \begin{flalign*}
      r) \qquad a &\equiv a \pmod{m}, &\\
      s) \qquad a &\equiv b \pmod{m} \quad \Rightarrow \quad b \equiv a \pmod{m}, &\\
      t) \qquad a &\equiv b \pmod{m},\quad b \equiv c \pmod{m} \quad \Rightarrow \quad a \equiv c \pmod{m}
   \end{flalign*}
\end{anmerkung}

Die drei Eigenschaften \emph{r, s und t} werden auch als Reflexivität (r), Symmetrie (s) und Transitivität (t) bezeichnet und bilden, wenn sie zusammen auftreten, die Grundlage der sogenannten \emph{Äquivalenzrelationen} \autocite[S. 8]{Wille2003Repetitorium}. Es ist also eine spezielle Form der Beziehung, welche die Gleichheit zweier Objekte in Bezug auf eine bestimmte Eigenschaft ausdrückt. Im Falle der Kongruenz ist dies die eingangs beschriebe Eigenschaft, dass die Zahlen $n_1$ und $n_2$ bei der Division durch die Zahl $m$ den selben Rest ergeben. 

\subsubsection{Restklassen Modulo m}
\label{subsubsec:Modulare Arithmetik - Restklassen}
Fasst man alle Elemente einer Menge, die dieselbe Eigenschaft wie $n_1$ oder $n_2$ aufweisen, d.h. alle Elemente, die in einer Äquivalenzrelation zu den beiden stehen, zusammen, so erhält man eine \emph{Äquivalenzklasse}. Äquivalenzklassen sind formal \autocite[S. 8]{Wille2003Repetitorium} definiert durch:

\begin{definition} (\emph{Äquivalenzklassen.})
\label{def:Äquivalenzklasse}
$\sim$ sei eine Äquivalenzrelation auf A. Die Teilmengen
\begin{equation}
   n_1/_\sim := \{n_2 \in A | n_2 \sim n_1\} \quad (n_1 \in A)
\end{equation}
von A heißen \textbf{Äquivalenzklassen} der Relation $\sim$.
\end{definition}
%
Da im Falle der Kongruenz der Wert des Rests als gemeinsame Eigenschaft betrachtet wird, ist die Anzahl der Äquivalenzklassen durch die Anzahl verschiedener Restwerte begrenzt. Sie werden deshalb auch als \emph{Restklassen} bezeichnet. Die Menge der möglichen Ergebnisse für ${n \bmod m}$ $(n,m \in \mathbb{Z})$ umfasst dabei die Elemente ${0, 1, ... , (m-1)}$, womit stets $m$ verschiedene Restklassen existieren \autocite[S. 10]{Wille2003Repetitorium}. Die zu einem Element ${n \in \mathbb{Z}}$ gehörige Restklasse modulo $m$ wird mit $\bar{n}$ notiert \autocite[S. 87]{Bundschuh2008Einfuehrung}.

\subsection{Elliptische Kurven}
\label{subsec:Mathematische Grundlagen - EC}
Die Mathematik der elliptischen Kurven ist, trotz ihrer erst relativ kurzen Anwendung in der Kryptographie (erstmals - unabhängig voneinander - durch \textcite{Koblitz1987Elliptic} und \textcite{Miller1985Use}), bereits seit einigen Jahrhunderten bekannt und wurde schon maßgeblich von Mathematikern wie Newton oder Poincaré geprägt \autocite[Abschn. 4]{Brown2009Three}. Seit ihrer Einführung in die Kryptographie bilden die Kurven, zusammen mit dem auf ihnen definierten \gls{ECDLP}\footnote{Elliptic Curve Discrete Logarithm Problem \gls{ECDLP}}, die Grundlage der \acrlong{ECC} (vgl. \cref{subsec:Mathematische Grundlagen - ECDLP,subsec:Kryptographische Grundlagen - ECC}).

Da es sich bei den elliptischen Kurven in ihrer Vielfalt um ein komplexes Themengebiet handelt, welches erweiterte Kenntnisse, sowohl in analytischer Geometrie als auch in Algebra erfordert, wird zum Zweck einer möglichst kurz gehaltenen Einführung auf die umfangreichen Ausführungen von \textcite[Kapitel 3]{Hankerson2006Guide} verwiesen. Darin wird unter anderem auch die Weierstraß-Normalform der elliptischen Kurven durch die folgende, frei übersetzte, Definition \autocite[S 76, Definition 3.1]{Hankerson2006Guide} und die anschließenden Anmerkungen \autocite[S 77, Anmerkungen 3.2]{Hankerson2006Guide} eingeführt. Sie kann als Basis für eine detailliertere Analyse der Mathematik der elliptischen Kurven verwendet werden, welche jedoch außerhalb des Rahmens dieser Arbeit liegt. Die anschließenden Abschnitte liefern deshalb nur eine kurze Einführung in die grundlegende Theorie der elliptischen Kurven, Rechenoperationen auf elliptischen Kurven und in die Besonderheiten von elliptischen Kurven über endlichen Körpern\footnote{Der Begriff \emph{Körper} bezeichnet ein Teilgebiet der Algebra. Es handelt sich hierbei um Mengen, welche die in den sogenannten Körperaxiomen definierten Eigenschaften besitzen. Diese Eigenschaften besagen, vereinfacht ausgedrückt, dass sowohl für die Addition als auch für die Multiplikation der Elemente der Mengen (für die Multiplikation ist die Null ausgeschlossen) das Assoziativ-, das Kommutativ- und das Distibutivgesetz gelten, ein neutrales Element definiert und die Existenz von Inversen sicher gestellt ist \autocite[S. 180 -  Körperaxiome]{Wille2003Repetitorium}.}.

\begin{definition} (\emph{Weierstraß-Normalform}):
\label{def:Weierstrass}
Eine elliptische Kurve $E$ über einem Körper $K$ ist definiert durch die Gleichung
%
   \begin{equation} \label{eq:Weierstrass}
      E: y^2 + a_1xy + a_3y = x^3 + a_2x^2 + a_4x + a_6
   \end{equation} 
%   
Die \emph{Diskriminante} von $E$ und wird im Folgenden mit $\Delta$  beschrieben und ist wie folgt definiert:
%
   \begin{equation}
      \left.\begin{aligned}
         \Delta &= -d^2_2d_8 - 8d^3_4 - 27d^2_6 + 9d_2d_4d_6 \\
         d_2 &= a^2_1 + 4a_2 \\
         d_4 &= 2a_4 + a_1a_3 \\
         d_6 &= a^2_3 + 4a_6 \\
         d_8 &= a^2_1a_6 + 4a_2a_6 - a_1a_3a_4 + a_2a^2_3 - a^2_4
      \end{aligned}\qquad\right\rbrace
   \end{equation} 
%
für $a_i \in K, i \in \{1...6\}$ und $ \Delta \neq 0$. Wenn $L$ ein beliebiger Erweiterungskörper von $K$ ist, dann ist die Menge der L-\emph{rationalen Punkte} auf $E$ gegeben durch
% 
   \begin{equation}
      E(L) = \{ (x, y) \in L \times L : y^2 + a_1xy + a_3y = x^3 + a_2x^2 + a_4x + a_6\} \cup \{ \infty \}
   \end{equation}%
%   
   wobei $\infty$ als der \emph{Punkt im Unendlichen} bezeichnet wird.
\end{definition}
%
\begin{anmerkung} \emph{(Kommentare zur \cref{def:Weierstrass})}
\label{anm:Weierstrass}
%
   \begin{enumerate}[label=(\roman*)]
      \item \cref{eq:Weierstrass} wird als Weierstraß-Gleichung bezeichnet.

      \item Wir sagen $E$ ist \emph{definiert über} $K$, weil die Koeffizienten $a_i$ in der \cref{eq:Weierstrass}, entsprechend ihrer Definition, Elemente von $K$ sind. Wir schreiben $E / K$ um so hervorzuheben, dass $E$ über $K$ definiert ist, wobei $K$ als Unterkörper bezeichnet wird. Man beachte, dass wenn $E$ über $K$ definiert ist, $E$ ebenfalls über jeder Körpererweiterung von $K$ definiert ist.

      \item Die Bedingung $\Delta \neq 0$ stellt sicher, dass die elliptische Kurve \glqq glatt\grqq{} ist, was bedeutet, dass kein Punkt auf der Kurve mit zwei oder mehr Tangenten existiert.

      \item Der Punkt $\infty$ ist der einzige Punkt auf der \emph{Gerade im Unendlichen}, welcher die projektive Form der Weierstraß-Gleichung erfüllt. Dies wird in \autocite[Abschn. 3.2]{Hankerson2006Guide} genauer erläutert.

      \item Die L-rationalen Punkte auf $E$ sind die Punkte $(x, y)$, welche die Gleichung der Kurve erfüllen und deren Koordinaten $x$ und $y$ zu $L$ gehören. Der Punkt im Unendlichen wird als L-rationaler Punkt für alle Erweiterungskörper $L$ über $K$ betrachtet.
   \end{enumerate}
\end{anmerkung}

\begin{figure}[h!bt]
   \centering
   \begin{minipage}{0.45\textwidth}
      \includegraphics[keepaspectratio, width=\textwidth]{EC_Kleiderbuegel.pdf}
      \centering
      (a) Elliptische Kurve vom Typ \glqq Kleiderbügel\grqq : ${y^2 = x^3 - 6x + 9, \quad x,y \in \mathbb{R}}$
   \end{minipage}
   \hfill
   \begin{minipage}{0.45\textwidth}
      \includegraphics[keepaspectratio, width=\textwidth]{EC_Insel.pdf}
      \centering
      (b) Elliptische Kurve vom Typ \glqq Insel\grqq : ${y^2 = x^3 - 7x + 5, \quad x,y \in \mathbb{R}}$
   \end{minipage}
   \mycaption{Stereotypische Verläufe von elliptischen Kurven über $\mathbb{R}$}{Die Abbildungen zeigen zwei Vertreter der elliptischen Kurven über $\mathbb{R}$, mit ihren beiden verschiedenen stereotypischen Verläufen. In (a) ist der einem Kleiderbügel ähnelnde Verlauf der Kurve ${y^2 = x^3 - 6x + 9, \quad x,y \in \mathbb{R}}$ abgebildet. Die rechts stehende Abbildung (b) zeigt die Kurve ${y^2 = x^3 - 7x + 5, \quad x,y \in \mathbb{R}}$, welche an die Darstellung einer Insel in Küstennähe erinnert.}
   \label{fig:EC_plots}
\end{figure}

\subsubsection{Elliptische Kurven über $\mathbb{R}$}
Einen einfachen Einstieg in die den elliptischen Kurven zu Grunde liegende Mathematik bietet die \emph{vereinfachte Weierstraß-Normalform}. Entsprechend dieser Darstellungsform wird eine elliptische Kurve $E$ über dem Körper der reellen Zahlen $E(\mathbb{R})$ beschrieben\footnote{Die allgemeine Form der vereinfachten Weierstraß-Normalform definiert die Gleichung über einem beliebigen Körper $K$, mit einer Charakteristik $char(K) \neq 2,3$. Der Wert der Charakteristik eines Körpers gibt dabei Aufschluss über die kleinste Anzahl an Wiederholungen die notwendig sind, um das multiplikativ neutrale Element (z.B. $1$ für $\mathbb{R}$ mit $x * 1 = x;  x, 1 \in \mathbb{R}$) mit sich selbst zu addieren und so das additiv neutrale Element (z.B. $0$ für $\mathbb{R}$ mit $x + 0 = x; x, 0 \in \mathbb{R}$) zu erhalten. Unendliche Körper haben die Charakteristik $char(K) = 0$. \autocite[S. 87ff]{Bosch2009Algebra} Das im Text angegebene Beispiel verwendet den Körper der reellen Zahlen beispielhaft.}, durch die Menge aller Punkte $(x, y) \in \mathbb{R}^2$, welche die Gleichung
%
\begin{equation}
   y^2 = x^3 + ax + b \qquad a, b \in \mathbb{R}
\end{equation}
%
erfüllen \autocite[S. 1, Theorem 1]{Koblitz1987Elliptic}. Darüber hinaus muss die Nebenbedingung ${4a^3 + 27b^2 \neq 0}$, d.h. $\Delta \neq 0$ für die Koeffizienten $a$ und $b$ erfüllt sein, so dass es keine Selbstüberschneidungen innerhalb der Kurve gibt. Eine solche Kurve ohne Selbstüberschneidungen wird auch als glatt oder \glqq nicht singulär\grqq{} bezeichnet \autocite[S. 78, Theorem 3.4]{Hankerson2006Guide}. Die Nicht-Singularität einer Kurve ist ein wichtiges Merkmal für das Rechnen auf den elliptischen Kurven im Rahmen der \gls{ECC}, welches im Unterabschnitt \nameref{subsubsec:Elliptische Kurven - Rechnen auf elliptischen Kurven} genauer erläutert wird.

Ergänzt wird die Menge der Punkte, welche die Kurve definieren, durch den sogenannten \glqq Punkt im Unendlichen\grqq{}, welcher z.B. durch das Symbol $\infty$ dargestellt wird und als neutrales Element der Menge fungiert \autocite[S. 13]{Hankerson2006Guide}. Dieser zusätzliche Punkt ist notwendig, um die grundlegenden Rechenoperationen, Addition zweier Punkte und Punkt-Verdopplung auf Basis einer perspektivischen Projektion der Kurven, zu ermöglichen\footnote{Siehe auch \autocite{Koblitz1987Elliptic} oder \autocite{Hankerson2006Guide} für weiterführende Erläuterungen.}. Die Abbildungen \ref{fig:EC_plots} (a) und (b) zeigen beispielhaft die Verläufe der Kurven ${y^2 = x^3 - 6x + 9}$ und ${y^2 = x^3 - 7x + 5}$ in vereinfachter Weierstraß-Normalform. Die Darstellungen in \ref{fig:EC_plots_endlich} zeigen die selben Kurven, jedoch in diesem Fall nicht über dem Körper der reellen Zahlen $\mathbb{R}$ definiert, sondern über dem Primkörper $\mathbb{F}_{101}$ (siehe Abschnitt \ref{subsubsec:Elliptische Kurven - modulare Arithmetik}).

Betrachtet man die Definition der vereinfachten Weierstraß-Normalform, so fällt auf, dass elliptische Kurven mathematisch ausgedrückt als kubische, ebene algebraische Kurven beschrieben werden können, welche symmetrisch zur x-Achse verlaufen. Dies bedeutet zum einen, dass die elliptischen Kurven durch ein Polynom, welches durch zwei unabhängige Variablen (x, y) beschrieben wird, definiert sind. Zum anderen beschreibt es, dass die elliptischen Kurven \glqq Kurven 3. Grades\grqq{} sind, d.h. dass die höchste in ihren Parametern auftretende Potenz stets $3$ beträgt \autocite[S. 201]{Wille2003Repetitorium}. Darüber hinaus lässt die Bezeichnung \glqq vereinfachte Weierstraß-Normalform\grqq{} auf die Existenz weiterer Darstellungsformen schließen. So finden neben ihr und der oben stehend bereits eingeführten allgemeinen Weierstraß-Normalform auch Montgomery-Kurven \autocite{Montgomery1987Speeding}, sowie Edwards- \autocite{Edwards2007Normal} und Twisted-Edwards-Kurven \autocite{Bernstein2008Twisted} Anwendung in der modernen Kryptographie\footnote{Siehe auch \cref{subsec:Stand der Technik - Kurven} - \nameref{subsec:Stand der Technik - Kurven} für eine Auswahl bekannter benannter Kurven der einzelnen Darstellungsformen.}.

\begin{figure}[t!]
   \centerline{\includegraphics[keepaspectratio, width=0.65\textwidth]{EC_Negate.pdf}}
   \mycaption{Negation eines Punktes}{Die Negation eines Punktes $P$ wird durch eine Spiegelung des Punktes entlang der x-Achse erreicht.}
   \label{fig:EC_Negierung}  
\end{figure}

\begin{figure}[t!]
   \centerline{\includegraphics[keepaspectratio, width=0.65\textwidth]{EC_Addition_Normal.pdf}}
   \mycaption{Addition zweier Punkte}{Um zwei voneinander unterschiedliche Punkte $P$ und $Q$ zu addieren, wobei keiner der beiden Punkte der Punkt im Unendlichen ($\infty$) oder das Negativ (${Q \neq -P}$) des anderen ist, wird eine Gerade durch die beiden Punkte gezogen. Der dritte Schnittpunkt mit der Kurve ist das Negativ des gesuchten Ergebnis $R$.}
   \label{fig:EC_Addition}
\end{figure}

\subsubsection{Rechnen auf elliptischen Kurven über $\mathbb{R}$ durch geometrische Betrachtung}
\label{subsubsec:Elliptische Kurven - Rechnen auf elliptischen Kurven}
Dieser Abschnitt behandelt das Rechnen auf elliptischen Kurven. Hierbei sind die Kurven in den folgenden Erläuterungen nicht ausschließlich über dem Körper der reellen Zahlen sondern über einem beliebigem Körper $K$ definiert. Eine entsprechende Kurve wird, den Anmerkungen \ref{anm:Weierstrass} folgend, auch mit $E / K$ oder $E(K)$ bezeichnet. Man schreibt $+$ als Addition zweier Punkte auf der elliptischen Kurve $E(K)$.

Die wichtigsten Rechenoperationen der elliptischen Kurven lassen sich auf Basis ihrer geometrischen Darstellung veranschaulichen. Für sie sind in \textcites[204]{Koblitz1987Elliptic}[79f]{Hankerson2006Guide} die folgenden Festlegungen beschrieben, welche auch in den \cref{fig:EC_Negierung,fig:EC_Addition,fig:EC_Doppel,fig:EC_PAI} dargestellt sind.

\enlargethispage{\baselineskip}

\paragraph{Negieren} Die Negation eines Punktes $P_1 \in E(K)$ mit $P_1 = (x, y)$ ist der Punkt $P_2 \in E(K)$ mit den Koordinaten $P_2 = (x, -y)$, d.h. entsprechend der vorhandenen Symmetrie zur x-Achse der zweite Punkt mit der selben x-Koordinate wie $P_1$ (siehe auch Abbildung \ref{fig:EC_Negierung}). Alternativ kann die Negation eines Punktes $P \in E(K)$ äquivalent zum Vorgehen der Addition erklärt werden (vgl. nächsten Abschnitt). Hierbei wird eine Gerade $\overline{P\infty}$ durch den Punkt $P$ und den Punkt im Unendlichen $\infty$ gezogen und der resultierende dritte Schnittpunkt ist der Punkt $-P$. Somit lässt sich die Negation auch als Gleichung $P + \infty = -P$ mit $P, \infty, -P \in E(K)$ ausdrücken, was auch in Abbildung \ref{fig:EC_PAI} dargestellt wird \autocite[S. 204]{Koblitz1987Elliptic}.

\paragraph{Addition zweier verschiedener Punkte} Wenn die Summe zweier Punkte auf einer elliptischen Kurve bestimmt werden soll, können verschiedene Fälle auftreten, welche gesondert voneinander betrachten werden müssen (vgl. Negieren, Punktverdopplung und das Neutrale Element). Der Normalfall behandelt die Addition zweier, voneinander verschiedener Punkte $P + Q = R$ mit $P, Q, R \in E(K)$ und $P \neq Q$, wobei keiner der Punkte $P$ und $Q$ das Negativ des anderen $P \neq -Q$ oder der Punkt im Unendlichen $P, Q \neq \infty$ ist. Dieser Fall ist in Abbildung \ref{fig:EC_Addition} dargestellt. Hierbei werden die beiden zu addierenden Punkte $P$ und $Q$ durch eine Gerade $\overline{PQ}$ verbunden. Der resultierende dritte Schnittpunkt mit der Kurve ist das negativ $-R$ des gesuchten Ergebnisses $R$ \autocite[S. 79]{Hankerson2006Guide}.
   
\paragraph{Punktverdopplung} Die Punktverdopplung ist ein Spezialfall der Punktaddition. Da in diesem Fall die Gerade $\overline{PQ}$ nicht durch zwei unabhängige Punkte gegeben ist, wird für die Punktverdopplung $P + Q = R = 2P$ mit $P, Q, R \in E(K), P = Q$ und $P, Q \neq \infty$ die Tangente im Punkt $P$ verwendet (vgl. Abbildung \ref{fig:EC_Doppel}). Der so erzeugte zweite Schnittpunkt $-R$ auf der Kurve ist das Negativ des gesuchten Ergebnisses $R$. Durch die hier verwendete Tangente wird auch die eingangs definierte Notwendigkeit einer glatten bzw. nicht singulären Kurve verdeutlicht, da ohne diese Eigenschaft Sonderfälle existieren würden in denen keine eindeutige Tangente im Punkt $P$ definiert ist \autocite[S. 79]{Hankerson2006Guide}.

\begin{figure}[tb]
   \centerline{\includegraphics[keepaspectratio, width=0.65\textwidth]{EC_Addition_Doubling.pdf}}
   \mycaption{Punktverdopplung}{Bei der Verdopplung des Punktes $P$ wird die Tangente im Punkt $P$ als notwendige Gerade verwendet. Der Schnittpunkt zwischen Gerade und Kurve ist das Negativ ($-R$) des Punktes $2P$ oder hier $R$.}
   \label{fig:EC_Doppel}
\end{figure}

\begin{figure}[tb]
   \centerline{\includegraphics[keepaspectratio, width=0.65\textwidth]{EC_PAI.pdf}}
   \mycaption{Das neutrale Element}{Das neutrale Element der elliptischen Kurven ist der abstrakte Punkt im Unendlichen. Er dient als notwendiger dritter Schnittpunkt falls es zur Addition eines Punktes mit seinem Negativ kommt. ${P + (-P) = 0}$.}
   \label{fig:EC_PAI}   
\end{figure}

\paragraph{Das neutrale Element} Das neutrale Element der elliptischen Kurve $E(K)$ ist der zusätzlich zur Definition hinzugefügte Punkt im Unendlichen $\infty$. Dieser wird verwendet, falls es bei der Addition oder Punktverdopplung zu keinem weiteren Schnittpunkt kommt und dient gleichzeitig der Definition der Negierung auf Basis der Addition (vgl. Abbildung \ref{fig:EC_PAI}). Hierbei gelten $P + ( -P )= \infty$ und $P + \infty = P$ \autocite[S. 80]{Hankerson2006Guide}.

\paragraph{Skalarmultiplikation} Die Skalarmultiplikation $nP$ für $P \in E(K)$ kann durch eine einfache n-malige Wiederholung der Punktaddition $nP = P + ... + P $ erzielt oder durch eine Kombination von Punktverdopplung und Punktaddition abgebildet werden \autocite[S. 204]{Koblitz1987Elliptic}.

\begin{figure}[bt]
   \centering
   \begin{minipage}{0.35\textwidth}
      \includegraphics[keepaspectratio, width=\textwidth]{EC_Kleiderbuegel_endlich.pdf}
      \centering
      (a) Elliptische Kurve: ${y^2 = x^3 - 7x + 5, \quad x,y \in \mathbb{F}_{101}}$
   \end{minipage}
   \hfill
   \begin{minipage}{0.35\textwidth}
      \includegraphics[keepaspectratio, width=\textwidth]{EC_Insel_endlich.pdf}
      \centering
      (b) Elliptische Kurve: ${y^2 = x^3 - 7x + 5, \quad x,y \in \mathbb{F}_{101}}$
   \end{minipage}
   \mycaption{Zwei elliptische Kurven über $\mathbb{F}_{101}$}{Die Abbildungen zeigen die Darstellungen zweier elliptischer Kurven über dem Primkörper $\mathbb{F}_{101}$. Es handelt sich hierbei um dieselben Kurvengleichungen, welche bereits in den Abbildungen \ref{fig:EC_plots} (a) und (b) dargestellt sind. Durch die Modulare-Reduktion auf Basis des Primkörpers ist ihre Form jedoch nicht mehr erkennbar. Beide Darstellungen entsprechen in ihrer Form eher zufälligen Punkteverteilungen.}
   \label{fig:EC_plots_endlich}
\end{figure}

\subsubsection{Elliptische Kurven über endlichen Körpern}
\label{subsubsec:Elliptische Kurven - modulare Arithmetik}
Ein endlicher Körper, auch als Galoiskörper\footnote{Der Galoiskörper ist nach dem französischen Mathematiker Évariste Galois benannt.} bezeichnet, beschreibt eine endliche Menge, welche alle Eigenschaften eines Körpers besitzt. Handelt es sich bei dieser Menge um die Restklassen der Primzahl $p$ (vgl. \cref{subsubsec:Modulare Arithmetik - Restklassen}), so wird der entsprechende Körper auch als Primkörper bezeichnet und in der Form $\mathbb{F}_p$ geschrieben. Er enthält die ganzen Zahlen ${\{0, 1, 2, ..., p - 1\}}$ und hat eine Charakteristik von ${char(\mathbb{F}) = p}$ \autocite[S. 26]{Hankerson2006Guide}.

Ist eine elliptische Kurve $E$ über einem Primkörper $\mathbb{F}_p$ ($p$ sei prim) definiert, wird zur Berechnung der Punkte, die Gleichung der Kurve um die Bestimmung des \emph{modulo $p$} erweitert. So wird zum Beispiel die in Abbildung \ref{fig:EC_plots} (a) dargestellte Kurve, mit der Gleichung ${y^2 = x^3 - 6x + 9}$, über dem Primkörper $\mathbb{F}_{101}$ zu ${y^2 = x^3 - 6x + 9 \pmod{101}}$ mit $x,y \in \mathbb{F}_{101}$ ergänzt. Es sind also nur noch Punkte in ${E / \mathbb{F}_{101}}$ enthalten, welche durch die Lösung der angepassten Gleichung berechnet werden können (siehe Abbildung \ref{fig:EC_plots_endlich} (a)). Auffällig ist, dass die originale Kurvenform nicht mehr erkennbar ist und die Darstellung eher einer zufälligen Punktverteilung ähnelt. Man spricht hierbei auch von einer \emph{Modulo-Reduktion} \autocite[S. 26]{Hankerson2006Guide}.

Wichtig ist, dass die im vorhergehenden Abschnitt beschriebenen Rechenoperationen in ihrer (hier nicht weiter erläuterten) algebraischen Form weiterhin ihre Gültigkeit behalten, auch wenn eine entsprechende geometrische Betrachtung nicht mehr möglich ist (vgl. hierzu: \autocite[S. 80ff]{Hankerson2006Guide}).

\subsection{Einwegfunktionen}
\label{subsec:Mathematische Grundlagen - Einwegfunktion}
Eine Einwegfunktion ist eine besondere mathematische Funktion, deren Ergebnis einfach zu berechnen, deren Umkehrung dagegen außergewöhnlich \glqq schwer\grqq{} zu bestimmen ist. Das heißt sei $x$ gegeben, kann $f(x)$ schnell bestimmt werden, sollte jedoch $f(x)$ gegeben sein, so dauert die Berechnung von $x$ mitunter \glqq ewig\grqq . Hierbei wächst die Rechenzeit, welche für die Bestimmung der Umkehrung benötigt wird, basierend auf der Größe $x$, weit stärker als die notwendige Rechenzeit der eigentlichen Funktion \autocite[Abschn. 2.3]{Schneier1996Applied}. Für die Unterscheidung zwischen \glqq effizient\grqq{} und \glqq schwer\grqq{} bzw. \glqq machbar\grqq{} und \glqq unmachbar\grqq{} wird hierfür zumeist betrachtet, ob das Problem in \emph{Polynomialzeit} lösbar ist, also ob es für die Funktion einen bekannten Algorithmus gibt, dessen Berechnungszeitobergrenze maximal polynomiell mit der Länge der Eingabegröße $n$ wächst. Dies wird, entsprechend der \emph{Landau}-Notation, auch als ${T(n) = O(n^k)},\ k = konstant$ ausgedrückt \autocite[S. 285]{Papadimitriou1994Computational}.

Wichtig ist in diesem Zusammenhang, dass der mathematische Beweis der Existenz von Einwegfunktionen ein noch ungelöstes Problem in der Informatik darstellt \autocite[Abschn. 2.3]{Schneier1996Applied}. Es wird also bei entsprechenden Funktionen nur angenommen, dass ihre Berechnung nicht in Polynomialzeit möglich ist. Die Zukunft könnte jedoch das Gegenteil beweisen, wodurch die zu Grunde liegende Sicherheit der Kryptographieverfahren, welche z.B. auf Problemen, wie dem \gls{DLP}\footnote{Discrete Logarithm Problem \gls{DLP}} oder dem im folgenden Abschnitt beschriebenen \gls{ECDLP}-basieren, nachhaltig gestört wäre \autocite[Abschn. 19.3 \& Tabelle 7.7]{Schneier1996Applied}. Gleichzeitig ist es schwer bis unmöglich vorauszusehen, welche zukünftigen Entwicklungen in den Bereichen Rechenkapazität und Parallelisierung erreicht werden.

\subsection{Der diskrete Logarithmus in elliptischen Kurven}
\label{subsec:Mathematische Grundlagen - ECDLP}
Das als \gls{ECDLP} abgekürzte Problem des diskreten Logarithmus in elliptischen Kurven ist eine Übertragung des diskreten Logarithmus Problems\footnote{Das Problem des diskreten Logarithmus beschreibt die Schwierigkeit der Berechnung des diskreten Logarithmus in der modularen Arithmetik. Ziel ist es hierbei, basierend auf dem Ergebnis der Berechnung ${a^x \mod n = b}$ den Wert für $x$ zu bestimmen, so dass die folgende Gleichung erfüllt wird: $a^x \equiv b \pmod{n}$. Dieses Problem ist unter anderem die Grundlage für den Diffie-Hellman-Schlüsselaustausch, den \glqq Digital Signature\grqq -Algorithmus, das ElGamal-Verfahren und ist nah Verwandt mit dem Problem, welches die Sicherheit von \gls{RSA}-verschlüsselten Nachrichten gewährleistet \autocite[Abschn. 11.6]{Schneier1996Applied}.} in die Mathematik der elliptischen Kurven und dient als Sicherheitsgrundlage der \gls{ECC}. Es wird von \textcite[Abschn. 4]{Koblitz1987Elliptic} beschrieben als:
%
\begin{definition} (Elliptic Curve Discrete Logarithm Problem).
Gegeben sei eine elliptische Kurve $E$ über $GF(q)$ und zwei Punkte $P, Q \in E$. Bestimme eine ganze Zahl $x$, sofern diese existiert, so dass die Gleichung $Q = xP$ gilt.
\end{definition}
%
Es basiert somit, genauso wie das \gls{DLP}, auf der Berechnung einer sogenannten \glqq Einwegfunktion\grqq . Entsprechend des oben stehenden Abschnitts \ref{subsec:Mathematische Grundlagen - Einwegfunktion} sind dieselben theoretischen Vor- und Nachteile vorhanden, die allen einwegfunktionsbasierten Verschlüsselungen gemein sind.
