%
% Beuth Hochschule für Technik --  Abschlussarbeit
%
% Kryptographische Grundlagen 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Kryptographische Grundlagen}
Im folgenden Abschnitt werden einige ausgewählte kryptographische Themen und Algorithmen eingeführt. Hierzu zählt die grundlegende Theorie der (blinden) digitalen Signatur, eine Übersicht über das \gls{RSA}-Verfahren, die \acrlong{ECC} und eine Auswahl verbreiteter \gls{ECC}-basierter Algorithmen.

\subsection{Digitale Signaturen}
\label{subsec:Kryptographische Grundlagen - Signaturen}
Die digitale Signatur ist das elektronische Pendant zur händischen Unterschrift. Sie gehörte \citeyear{Diffie1976New} zu den Grundanforderungen, welche von \citeauthor{Diffie1976New} an die von ihnen präsentierte Idee der \acrlong{PKC} aufgestellt wurden \autocite[S. 1]{Diffie1976New}. Ihr Ziel war es, eine Form der elektronischen Kommunikation zu schaffen, die es dem Urheber einer Nachricht erlaubt, diese so zu signieren, dass die Echtheit der Nachricht und der zugehörige Absender von jeder beliebigen Person überprüft werden kann, ohne dass es möglich ist, auf Basis der öffentlichen Informationen, eine Nachricht im Namen eines anderen zu verfassen. Wodurch für beide Parteien sicher gestellt wird, dass die Urheberschaft einer Nachricht nicht nachträglich angezweifelt werden kann \autocite[S. 2f]{Diffie1976New}. Das zu Grunde liegende Prinzip der Erzeugung und Verifizierung einer digitalen Signatur sieht dabei wie folgt aus:
%
\begin{itemize}
   \item Der Signierende verschlüsselt die Nachricht (oder ihren Hashwert)\footnote{Der Hashwert einer Nachricht ist das Ergebnis einer kryptographischen Hashfunktion, in welcher die Nachricht als Eingabewert verwendet wurde. Bekannte Vertreter sind die Algorithmen der sogenannten MD4-Familie: MD5, SHA-1, SHA-2 \autocite[S. 304f]{Paar2009Understanding}.} mit seinem privaten Schlüssel.
   \item Er übermittelt die Nachricht an den gewünschten Empfänger.
   \item Der Empfänger verwendet den öffentlichen Schlüssel des Signierenden, um die Nachricht zu entschlüsseln und überprüft so die Echtheit der Signatur.
\end{itemize}
%
Der exakte Ablauf des Algorithmus und die Details können dabei, je nach verwendeten Verfahren, variieren. So wird im Rahmen des \gls{RSA}-Verfahrens für die Erzeugung einer Signatur der selbe Algorithmus wie zur Verschlüsselung verwendet, wohingegen in anderen Verfahren eigene Signaturalgorithmen, zum Beispiel der \emph{\acrlong{DSA}} oder der \emph{\acrlong{ECDSA}}, existieren, welche nicht für eine Verschlüsselung geeignet sind.

\begin{figure}[t!b]
   \centerline{\includegraphics[keepaspectratio, width=0.75\textwidth]{Blind_Signature.pdf}}
   \mycaption{Schematische Darstellung eines blinden Signaturverfahrens}{(1) Der Anfragende sendet die ausgeblendete, zu signierende Nachricht zusammen mit seinem Berechtigungsnachweis an den Signierenden. (2) Dieser überprüft die Berechtigung, unterschreibt die Nachricht und sendet sie zurück an den Anfragenden, welchem die originale Nachricht, nun mit gültiger Signatur, wieder eingeblendet wird.}
   \label{fig:Signaturschema}
\end{figure}

\subsubsection{Blinde Digitale Signaturen}
Eine spezielle Form der digitalen Signatur ist die blinde digitale Signatur. Sie wurde erstmals von \textcite{Chaum1983Blind, Chaum1985Security} vorgeschlagen und zeichnet sich dadurch aus, dass der Signierende nicht der Autor der zu unterschreibenden Nachricht ist und ihm ihr Inhalt auch nicht bekannt ist. Hierbei sollte jedoch über einen weiteren Kanal oder weiterführende Maßnahmen sichergestellt werden, dass dem Signierenden ausschließlich Nachrichten von autorisierten Personen vorgelegt werden. \autocite[S. 1]{Chaum1983Blind} Er beschreibt diesen Vorgang mit der Analogie eines Briefumschlags, welcher aus Durchschlagpapier besteht (siehe Abbildung \ref{fig:Signaturschema}). Steckt man die zu unterschreibende Nachricht, zum Beispiel einen ausgefüllten Stimmzettel in einer anonymen Briefwahl, in diesen Briefumschlag (Schritt 1), so kann, nach vorhergehender Prüfung der Stimmberechtigung durch einen Wahlhelfer, der Umschlag von außen signiert werden, ohne dass ihm der Inhalt der Nachricht bekannt wird (Schritt 2). Später entfernt der Wähler die Nachricht aus dem Briefumschlag und hat eine gültige Unterschrift des Wahlhelfers auf seinem Stimmzettel, den er jetzt einsenden kann \autocite[S. 2f]{Chaum1983Blind}.

Die wichtigste Eigenschaft, neben der namensgebenden \emph{Blindheit} des Signierenden, nach der ihm der Inhalt einer vorgelegten Nachricht nicht bekannt ist, ist in diesem Zusammenhang die \emph{fehlende Zurückführbarkeit} einer Nachricht. Durch sie wird sicher gestellt, dass der Signierende keine Möglichkeit hat den Zusammenhang zwischen einer ihm zur Unterschrift vorgelegten Nachricht und einer Nachricht mit seiner Signatur herzustellen. Wodurch ihm auch zu einem späteren Zeitpunkt kein Rückschluss auf den Inhalt einer Nachricht oder den tatsächlichen Zeitpunkt einer Unterschrift möglich ist \autocite[S. 2]{Chaum1983Blind}.

Diese Art der Signatur kann zum Beispiel für die Umsetzung anonymer Wahlen, digitaler Währungen (\autocite[S. 1044]{Chaum1985Security}) oder bei der Vergabe von Pseudonymen verwendet werden \autocite[Abschn. 5.3]{Schneier1996Applied}. \textcite{Asghar2011Survey} zeigt darüber hinaus, dass für viele der bekannten digitalen Signaturverfahren bereits Algorithmen veröffentlicht wurden, welche verwendet werden können, um blinde Signaturen zu erzeugen. Diese Arbeit beschäftigt sich hauptsächlich mit dem aktuellen Stand der Adaption blinder Signaturverfahren auf Basis der \acrlong{ECC} und im Speziellen mit Erweiterungen des im folgenden Abschnitt \ref{subsec:Kryptographiesche Grundlagen - ECDSA} erläuterten Algorithmus \gls{ECDSA}, welche in den Ausführungen von \textcite{Asghar2011Survey} nicht betrachtet werden.

\enlargethispage{1.5\baselineskip}

\subsection{RSA}
\label{subsec:Kryptographische Grundlagen - RSA}
Das \gls{RSA}-Verfahren ist ein nach seinen drei Autoren Ron (R)ivest, Adi (S)chamir und Leonard (A)dleman benannter Algorithmus aus dem Bereich der \acrlong{PKC} und wurde erstmals \citeyear{Rivest1978Method} in \autocite{Rivest1978Method} präsentiert. Nachdem erst zwei Jahre zuvor \citeyear{Diffie1976New} von \textcite{Diffie1976New} das Konzept der \acrlong{PKC} veröffentlicht wurde, handelte es sich hierbei um eines der ersten ausformulierten \gls{PKC}-Verfahren und bietet neben der reinen Verschlüsselung, auch die Möglichkeit der Erzeugung digitaler Signaturen (siehe auch \cref{subsec:Kryptographische Grundlagen - Signaturen} - \nameref{subsec:Kryptographische Grundlagen - Signaturen}) \autocite[Abschn. 19.3]{Schneier1996Applied}.

Der Ausgangspunkt für das Verfahren ist ein bisher ungelöstes zahlentheoretisches Problem, das nach aktuellem Wissensstand als mathematische Einwegfunktion betrachtet wird \autocite[S. 279ff]{Papadimitriou1994Computational}. Hierbei handelt es sich um das Problem der Primfaktorzerlegung, welches die Faktorisierung ganzer Zahlen in ihre zu Grunde liegenden Primfaktoren beschreibt. Es stellt somit die Basis für die \gls{RSA}-Schlüsselerzeugung dar, in welcher das öffentlich verwendete Modul $N$ durch die Multiplikation zweier geheimer Primzahlen $p$ und $q$ berechnet wird. Da $p$ und $q$ auch verwendet werden können, um auf Basis des öffentlichen Schlüssels auch den privaten Schlüssel zu bestimmen, wird es als die mathematische Grundlage der Sicherheit des RSA-Verfahrens betrachtet \autocite[Abschn. 11.4 \& 19.3]{Schneier1996Applied}.

Gleichzeitig definiert RSA selbst eine eigene Einwegfunktion, die dem Verfahren nach als RSA-Problem bezeichnet wird. Sie ähnelt dem \acrlong{DLP} bzw. dem im \cref{subsec:Mathematische Grundlagen - ECDLP} erläuterten \gls{ECDLP}. Das Problem beschreibt, entspechend den Ausführungen von \textcite[S. 1ff]{Rivest2005RSA} die Schwierigkeit in der Rekonstruktion einer Nachricht $M$ aus der verschlüsselten Nachricht $C = M^e \pmod{n}$, für den Fall, dass sowohl der verwendete öffentliche Schlüssel $(n\ ,e)$ als auch die verschlüsselte Nachricht $C$ bekannt sind.

Entsprechend der Ausführungen in \cref{subsec:Mathematische Grundlagen - Einwegfunktion} (\nameref{subsec:Mathematische Grundlagen - Einwegfunktion}) unterliegt die Sicherheit des \gls{RSA}-Verfahren den gleichen Limitierungen, welche generell durch die Verwendung von Einwegfunktionen resultieren. Dies bedeutet, dass die theoretische Möglichkeit besteht, dass es zur Entwicklung von Algorithmen kommt, durch die eine Lösung der hier verwendeten Probleme so stark vereinfacht wird, dass eine weitere Nutzung des Verfahrens anschließend unmöglich ist. Hinzu kommt, dass sowohl im Bereich der Primfaktorzerlegung, als auch im Bezug auf das RSA-Problem, Algorithmen veröffentlicht wurden, die zu einer Reduktion des benötigten Berechnungsaufwands geführt haben \autocite{Buchmann2000Sicher} und somit eine Anpassung der empfohlenen Schlüssellängen notwendig wurde. So rät das Bundesamt für Sicherheit in der Informationstechnik zwar aktuell noch Schlüssellängen von 2048 Bit \autocite[Abschn. Mechanismenstärke / Schlüssellänge]{BSI2013Grundschutz} zu verwenden, für mittel- und langfristige Sicherheit wird jedoch ein Umstieg auf Schlüssel mit Längen über 3000 Bit empfohlen \autocite[S. 36f]{BSI-TR-02102}. Vergleicht man diese Angaben mit ehemaligen Schätzungen des \gls{RSA}-Mitentwicklers Ron Rivest aus dem Jahre 1990 (siehe \autocite[Abschn. 2.3, Tabelle 7.8]{Schneier1996Applied}), zeigt sich, dass die Entwicklungen in diesem Bereich um einiges schneller voranschreiten als es vor 25 Jahren absehbar war.

\vspace{-0.2cm}
\enlargethispage{1.5\baselineskip}

\subsection{Elliptische-Kurven-Kryptographie}
\label{subsec:Kryptographische Grundlagen - ECC}
Die \acrlong{ECC} ist, dem Namen entsprechend, ein \acrlong{PKC}-Verfahren, dessen mathematische Grundlagen auf Verwendung elliptischer Kurven basiert (siehe \cref{subsec:Mathematische Grundlagen - EC}) und dessen Sicherheit durch den notwendigen Zeitaufwand in der Berechnung des Problems des diskreten Logarithmus in elliptischen Kurven gesichert ist (siehe \cref{subsec:Mathematische Grundlagen - ECDLP}). Es wurde erstmals von den beiden Mathematikern \textcite{Miller1985Use} und \textcite{Koblitz1987Elliptic} präsentiert, welche im Verlauf der 1980er Jahre unabhängig voneinander an dieser Thematik forschten.

Obwohl die \gls{ECC} oft direkt mit dem \gls{RSA}-Verfahren verglichen wird (vgl. \autocite{Maletsky2015RSA, NIST2016Recommendation}), werden unter dem Begriff \acrlong{ECC} nicht nur elliptische Kurven basierte, \gls{RSA}-äquivalente Verfahren zusammengefasst. Die \gls{ECC} hat sich viel mehr als Oberbegriff aller elliptischer Kurven basierter Verfahren etabliert. Hierzu zählen unter anderem der im folgenden \cref{subsec:Kryptographiesche Grundlagen - ECDSA} (\nameref{subsec:Kryptographiesche Grundlagen - ECDSA}) aufgeführte Algorithmus \gls{ECDSA} \autocite{Johnson2001Elliptic}, die Übertragung des Diffie-Hellman-Schlüsselaustauschs \gls{ECDH} \cite{Diffie1976New}, der von \acrlong{DJB} entworfene \gls{EdDSA} \autocite{Bernstein2012High} oder das hybride Verschlüsselungsverfahren \gls{ECIES} \autocite{Gayoso2010Survey}. Wichtig ist hierbei, dass all diesen Verfahren das der \gls{ECC} zu Grunde liegende \gls{ECDLP} als Sicherheitsgarantie zu eigen ist. Da es sich hierbei um eine Einwegfunktion handelt, bringt dies, wie bereits in den Abschnitten \ref{subsec:Mathematische Grundlagen - Einwegfunktion} und \ref{subsec:Kryptographische Grundlagen - RSA} angemerkt, eine mögliche Unsicherheit mit sich. Der Grund hierfür ist der bisher nicht vorhandene Existenzbeweis für Einwegfunktionen. Es gibt also keine Garantie, dass für das \gls{ECDLP} nicht doch ein Algorithmus existiert, welcher seine Berechnung trivialisiert (vgl. hierfür auch \autocite[Abschn. 2.3]{Schneier1996Applied}).

Da, verglichen mit den Veröffentlichungen im Bereich \gls{DLP} und Faktorisierung \autocites[S. 2-5]{Buchmann2000Sicher}[S.194f, S. 219-224]{Paar2009Understanding}, bisher nur wenige Fortschritte im Bezug auf die Geschwindigkeit der Lösung des \gls{ECDLP} gemacht wurden \autocites[S. 5f]{Buchmann2000Sicher}[S. 251f]{Paar2009Understanding}[S. 12f]{Diem2010Discrete}, wird kryptographischen Schlüsseln in \gls{ECC} basierten Verfahren ein höheres Sicherheitsniveau zugesprochen als \gls{RSA}-basierten Schlüsseln mit vergleichbarer Länge\footnote{Spricht man im Zusammenhang von kryptographischen Schlüssellängen von einem Sicherheitsniveau von $n$-Bit, so wird damit auf ein symmetrisches Verschlüsselungsverfahren mit der Schlüssellänge $n$ Bezug genommen. Wobei davon ausgegangen wird, dass die effektivste Angriffsmethode gegen dieses symmetrische Verfahren das Ausprobieren aller Lösungsmöglichkeiten (Brute-Force-Methode) ist. Da die Länge von kryptographischen Schlüsseln in Bit angegeben wird, wird auch ihr Sicherheitsniveau in Bit angegeben. \autocite[S. 14, \glqq bits of security\grqq]{NIST2016Recommendation}}. Hierzu enthält die Tabelle \ref{tab:Sicherheitsniveau ECC-RSA} eine Übersicht über die Sicherheitsniveaus von \gls{ECC}- und \gls{RSA}-basierten Schlüsseln. Sie ist der aktuellsten Revision einer vom \gls{NIST} veröffentlichten Empfehlung zur Schlüsselverwaltung entnommen \autocite{NIST2016Recommendation} und bietet einen guten Überblick über die aktuelle Situation. Vergleicht man die Sicherheitsniveaus mit den angegebenen Schlüssellängen, ist erkennbar, wie stark die benötigten Schlüssellängen bei gleichen Sicherheitsniveau auseinander driften.

\begin{table}[t!]
   \begin{minipage}{\textwidth}
      \center
      \begin{tabular}{ | c | c | c | c | c }
         \hline
         \textbf{\gls{ECC}-Schlüssellänge}\footnote{ECC-Schlüssellänge als Beispiel für Algorithmen wie \gls{ECDSA}} & \textbf{RSA-Schlüssellänge} & \textbf{ECC/RSA-Ratio} & \textbf{Sicherheit in Bit}\footnote{Die Sicherheit in Bit entspricht der Schlüssellänge eines symmetrischen Verfahrens wie zum Beispiel \emph{AES}.}\\
         \hline			
         192 & 1024 & 1:5,$\bar{3}$ & 80 \\
         224 & 2048 & 1:9,14 & 112 \\ 
         256 & 3072 & 1:12 & 128 \\
         384 & 7680 & 1:20 & 192 \\
         512 & 15360 & 1:30 & 256 \\
         \hline
      \end{tabular}
   \end{minipage}
   \mycaption{Vergleich des Sicherheitsniveaus von Schlüssellängen}{Die Tabelle vergleicht das Wachstum von \gls{ECC}- und \gls{RSA}-basierten Schlüsseln im Bezug auf das gebotene Sicherheitsniveau in Bit. Als Vergleich für das Sicherheitsniveau dient dabei die vegleichbare Schlüssellänge eines symmetrischen Verschlüsselungsverfahren wie AES. Es zeigt sich, dass \gls{RSA}-basierte Schlüssel einem deutlich stärkeren Wachstum unterworfen sind, um ein äquivalentes Sicherheitsniveaus zu erbringen. Begründen lässt sich dieses Verhalten mit den Fortschritten in der Lösung der \gls{RSA}-spezifischen Probleme (vgl. hierfür \autocite{Buchmann2000Sicher}; Quelle der Informationen: \autocite[Tabelle 2]{NIST2016Recommendation}).}
   \label{tab:Sicherheitsniveau ECC-RSA}
\end{table}

\subsubsection{Vorteile gegenüber dem \gls{RSA}-Verfahren}
Nimmt man die zuvor erläuterten unterschiedlichen Schlüssellängen als Grundlage, bietet die \acrlong{ECC} nach aktuellem Kenntnisstand einige Vorteile gegenüber dem \gls{RSA}-Verfahren, die vor allem auf Geräten mit beschränkter Rechen- und Speicherkapazität, wie den eingebetteten Systemen, eine Rolle spielt. Diese Vorteile werden im Folgenden beschrieben. Die dabei verwendeten Vergleichswerte entstammen den Ausführungen von \citeauthor{Maletsky2015RSA} in \autocite{Maletsky2015RSA}.

\paragraph*{Schlüsselerzeugung} Die Erzeugung von Schlüsseln verläuft unter Verwendung einer oder mehrerer Zufallszahlen, welche im Verlauf des jeweils spezifischen Algorithmus auf Basis einer Zufallszahlquelle erzeugt werden (siehe z.B. \cite[S. 23]{Johnson2001Elliptic}). Diese Zufallszahl wird zumeist unter Verwendung eines sogenannten kryptographisch sicheren Pseudozufallszahlengenerator (CSPRNG) erzeugt, welcher im Normalfall durch eine Menge von zufälligen Ereignissen gespeist wird \autocite[S. 34ff]{Paar2009Understanding}. Aufgrund der im Rahmen der \gls{ECC}-basierten Schlüsselerzeugung möglichen kürzeren Zielschlüssellängen, ist dieser Schritt weniger aufwendig als im \gls{RSA}-Verfahren. Darüber hinaus müssen im Verlauf des \gls{RSA}-Algorithmus zwei Primzahlen bestimmt werden, welche in ihrer Länge beide in etwa der halben der tatsächlichen Schlüssellänge entsprechen \autocite[S. 7f]{Rivest1978Method}. Die hierfür notwendigen Primzahltests benötigen bei großen Schlüssellängen entsprechend mehr Zeit. So wird von \textcite[S. 3]{Maletsky2015RSA} die \gls{RSA}-Schlüsselerzeugung als 100-1000 mal langsamer eingeschätzt als ein \gls{ECC}-basiertes Äquivalent, z.B. im Rahmen des \gls{ECDSA}.

\paragraph*{Verschlüsselung und Signaturerzeugung} Da entsprechend des \gls{PKC}-Prinzips, sowohl bei der Ver- und Entschlüsselung, als auch bei der Signaturerzeugung und -validierung, entweder der private oder der öffentliche Schlüssel verwendet wird, haben auch hier die kürzeren Schlüssellängen einen direkten Einfluss. So wird bei höheren Schlüssellängen im Rahmen der Berechnung des jeweiligen Vorgangs entsprechend mehr Rechenleistung bzw. -zeit benötigt, um zu einem Ergebnis zu gelangen. Laut \citeauthor{Maletsky2015RSA} ist die Verwendung von \gls{RSA} im 128-Bit-Sicherheitsniveau circa zehn mal langsamer als ein vergleichbarer \gls{ECC}-basierter Algorithmus. Der Unterschied zwischen den Verfahren steigert sich mit erhöhtem Sicherheitsniveau, so wird \gls{RSA} von \citeauthor{Maletsky2015RSA} im 256-Bit-Sicherheitsniveau bereits 50-100 mal langsamer eingestuft.

\paragraph*{Signatur- und Schlüsselübertragung} Stellt in einem Anwendungsfall die Bandbreite der Nachrichtenübertragung eine limitierte Ressource dar, so kann auch der Größe der verschlüsselten Nachrichten und erzeugten Signaturen eine wichtige Rolle zukommen, da sie direkt von der eingangs gewählten Schlüssellänge abhängt. So kommt \textcite[S. 3]{Maletsky2015RSA} zu dem Ergebnis, dass im Bereich des 128-Bit-Sicherheitsniveaus öffentliche Schlüssel und Signaturen in \gls{ECC}-basierten Algorithmen ungefähr sechs mal kleiner sind als im Rahmen von \gls{RSA}.

\subsubsection{Bedenken}
Trotzdem die \acrlong{ECC}, seit ihrer Einführung Mitte der 1980er Jahre, rege Aufmerksamkeit in der kryptographischen Forschung genießt und eine Reihe an Vorteilen gegenüber dem etablierten \gls{RSA}-Verfahren bietet, gibt es einige Bedenken in Bezug auf diese Art der \acrlong{PKC}, die sich nicht von der Hand weisen lassen. Hierzu zählen zum Beispiel die höhere mathematische Komplexität, erhöhte Angreifbarkeit durch zukünftige Entwicklungen in der Quantenkryptographie und die immer noch undurchsichtige Patentsituation. 

\paragraph*{Komplexität der Mathematik} Während die geometrischen Erläuterungen der grundlegenden Rechenoperationen auch ohne tiefgehende mathematische Grundkenntnisse schnell und verständlich erklärt werden können (vgl. \cref{subsubsec:Elliptische Kurven - Rechnen auf elliptischen Kurven} - \nameref{subsubsec:Elliptische Kurven - Rechnen auf elliptischen Kurven}), ist für das Verständnis der algebraischen Übertragung einiges an mathematischen Vorwissen notwendig. Aus diesem Grund entfällt in den einführenden Betrachtungen ihre Erläuterung (siehe auch \autocites[Abschn. 19.8]{Schneier1996Applied}[S. 242-249]{Paar2009Understanding}). Darüber hinaus, ist auch die Auswahl sicherer Kurvenparameter mit all den zu beachtenden Sonderfällen ein komplexes Problem \autocite{Bernstein2014Safe}.

\paragraph*{Quantenkryptogaphie} Betrachtet man die aktuell noch hypothetische Entwicklung von kryptographisch relevanten Quantencomputern, so werden, wie in \autocite[S. 182ff]{Yan2013Quantum} von \citeauthor{Yan2013Quantum} erläutert, die verwendeten kürzeren Schlüssellängen von \gls{ECC}-basierten Verfahren zu einem Nachteil gegenüber \gls{RSA} werden. Folgt man den Ausführungen in \autocite[S. 182f, Anm. 4.5 \& Tabelle 4.3]{Yan2013Quantum}, werden zwar im Verhältnis zur Schlüssellänge betrachtet mehr Qubits\footnote{Qubit ist eine Abkürzung für den Begriff Quantenbit. Es stellt die Grundlage der Quantencomputer dar und wird doch analog zum bekannten Bit verwendet. Eine mathematisch genauere Ausführung findet sich in \autocite[S. 17-21]{Yan2013Quantum}.} zur Lösung des \gls{ECDLP}, als zur Lösung des Faktorisierungsproblems benötigt. Auf Grund der kurzen Schlüssellängen ist die Trivialisierung  der \gls{ECDLP}-Berechnung trotzdem um einiges früher zu erwarten, da die Gesamtanzahl an benötigten Qubits geringer ist.

\paragraph*{Patentsituation}  Während das Patent auf das \gls{RSA}-Verfahren bereits zur Jahrtausendwende ausgelaufen ist \autocite[S. 174]{Paar2009Understanding}, ist die Patentsituation im Bereich der \gls{ECC} immer noch recht unübersichtlich \autocite[S. 253]{Paar2009Understanding}. Einen Eindruck bietet jedoch der Brief \autocite{Certicom2005List}, welcher von der Certicom Corp\footnote{Der Internetauftritt der Certicom Corp. kann über die URL \url{https://www.certicom.com/}. aufgerufen werden. (Zuletzt besucht am 26. März 2016)} an das SECG-Konsortium\footnote{Standards for Efficient Cryptography Group (SECG)} versendet wurde.

\subsection{Algorithmus für digitale Signaturen in elliptischen Kurven (ECDSA)}
\label{subsec:Kryptographiesche Grundlagen - ECDSA}
Der als \gls{ECDSA} bekannte Algorithmus für digitale Signaturen in elliptischen Kurven, ist eine erstmals \citeyear{Vanstone1992NIST} von \textcite{Vanstone1992NIST} vorgeschlagene Übertragung des \gls{DSA} zur Erzeugung sicherer digitaler Signaturen in die \acrlong{ECC}. Als Grundlage für die Sicherheit des Algorithmus verwendet \citeauthor{Vanstone1992NIST} das im \cref{subsec:Mathematische Grundlagen - ECDLP} vorgestellte \acrlong{ECDLP}.

Seit der Einführung des \gls{ECDSA} im Jahr \citeyear{Vanstone1992NIST} ist die Verbreitung des Algorithmus stetig angewachsen. So ist er seit einigen Jahren als ISO\footnote{International Standards Organization (ISO)} 14888-3, ANSI\footnote{American National Standards Institute (ANSI)} X9.62, IEEE\footnote{Institute of Electrical and Electronics Institute (IEEE)} P1363 und FIPS\footnote{Federal Information Processing Standard (FIPS)} 186-2 standardisiert \autocite[S. 3]{Johnson2001Elliptic} und wurde unter anderem offiziell in das OpenPGP-Nachrichten Format \autocite{RFC6637}, das \glqq Transport Layer Security\grqq{}-Protokol (TLS) \autocite{RFC4492} und das \glqq Secure Shell Transport Layer\grqq{} \autocite{RFC5656} integriert.

\subsubsection{Ablauf des ECDSA}
Voraussetzung für die Verwendung des \gls{ECDSA} ist nach \textcite[S. 16]{Johnson2001Elliptic} die Auswahl einer geeigneten elliptischen Kurve $E$ über einem endlichen Körper $\mathbb{F}_p$ mit $p = prim$ sowie die Wahl eines geeigneten Basispunkts ${G \in E(F_p)}$. Gleichzeitig ist es wichtig die kleinste ganze Zahl $n$ zu bestimmen, welche die folgende Gleichung erfüllt ${nG = \infty}$. Diese Zahl $n$ wird als Ordnung des Basispunkts $Q$ bezeichnet und sollte ebenfalls prim sein. Für die Wahl des Körpers, der Kurve und der Primzahl gelten dabei einige recht spezielle Bedingungen, welche notwendig werden, um den Algorithmus resistent gegen Angriffe auf bekannte Schwachpunkte zu machen. Zu den Angriffsalgorithmen zählen unter anderem \emph{Pollards rho} \autocite[S. 157]{Hankerson2006Guide} oder \emph{Pohlig-Hellman} \autocite[S. 155]{Hankerson2006Guide}. Die Bedingungen, gegen welche spezifischen Angriffe sie gedacht sind und wie man die notwendigen Werte auswählen kann, werden in \autocite[S. 16-23]{Johnson2001Elliptic} genauer erläutert.

Die Gesamtheit der Werte, welche notwendig sind um den Algorithmus zu verwenden, werden auch als kryptographische Parameter oder im englischen \glqq domain parameter\grqq{} einer elliptischen Kurve bezeichnet \autocite[S. 172]{Hankerson2006Guide}. Sie werden sowohl im Rahmen der Schlüssel- und Signaturerzeugung, als auch bei deren Signaturvalidierung benötigt und sollten deshalb zusammen mit der Signatur kommuniziert und auch vom Empfänger validiert werden \autocites[S. 21ff]{Johnson2001Elliptic}[S. 173ff]{Hankerson2006Guide}. Um diesen Prozess zu vereinfachen, können in diesem Zusammenhang die Kurven (und ihre Bezeichner) verwendet werden, welche in den oben genannten Standards definiert sind.

Sind die notwendigen Parameter festgelegt, wird entsprechend den auf der nächsten Seite folgenden, aus den Ausführungen in \cite[S. 23ff]{Johnson2001Elliptic} entnommenen, Algorithmen vorgegangen.

\clearpage
\paragraph{Schlüsselerzeugung} Ein \gls{ECDSA}-Schlüsselpaar besteht aus dem öffentlichen Punkt ${Q \in E(F_p)}$ und der geheimen Zahl $d$. Sie werden wie folgt bestimmt:

\begin{enumerate}
   \itemsep0.1em 
   \item Zuerst wird $d$ als zufällig ganze Zahl im Bereich [$1$, $n - 1$] ausgewählt.
   \item Anschließend wird $Q = dG$ berechnet. 
\end{enumerate}

\paragraph{Signaturerzeugung} Die Signatur einer Nachricht $m$ wird durch das Zahlenpaar $(r, s)$ angegeben und mit dem folgenden Algorithmus erzeugt:

\begin{enumerate}
   \itemsep0.1em 
   \item Zuerst wird eine zufällige ganze Zahl $k$ im Bereich [$1$, $n - 1$] bestimmt.
   \item Berechnung ${kG = (x_1, y_1)}$ und setze ${r = x_1 \bmod{n}}$. Falls $r = 0$ gilt, zurück zu Schritt 1.
   \item Berechne $k^{-1} \bmod{n}$.
   \item Berechne $e = SHA-1(m)$.
   \item Berechne $s = k^{-1}(e + dr) \bmod{n}$. Falls $s = 0$ gilt, beginne bei Schritt 1.
\end{enumerate}

\paragraph{Signaturvalidierung} Eine erhaltene Signatur $(r, s)$ einer Nachricht $m$ wird wie folgt validiert:

\begin{enumerate}
   \itemsep0.1em 
   \item Überprüfe, dass sich $r$ und $s$ im Bereich [$1$, $n - 1$] befinden.
   \item Berechne $e = SHA-1(m)$.
   \item Berechne $w = s^{-1} \bmod{n}$.
   \item Berechne $u_1 = ew \bmod{n}$ und $u_2 = rw \bmod{n}$.
   \item Berechne $X = (x_1, y_1) = u_1G + u_2Q$. Falls $X = \infty$ gilt, ist die Signatur ungültig.
   \item Setze $v = x_1 \bmod{n}$.
   \item Die Signatur ist nur gültig falls $v = r$ gilt.
\end{enumerate}
