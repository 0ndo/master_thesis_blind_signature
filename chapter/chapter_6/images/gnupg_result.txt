custom_keyring//pubring.kbx
---------------------------
pub   nistp256/6E7E4D7F 2015-12-29 [SC]
uid         [ unknown] ecc_nist_p_256
sig!3        6E7E4D7F 2015-12-29  ecc_nist_p_256

pub   rsa4096/290E99FF 2014-02-23 [SC] [expires: 2018-02-23]
uid         [ unknown] Bruno Kirschner <bruno.kirschner@online.de>
sig!3        290E99FF 2014-02-23  Bruno Kirschner <bruno.kirschner@online.de>
sig!1        6E7E4D7F 2017-05-29  ecc_nist_p_256
sub   rsa4096/ADCFD3F7 2014-02-23 [E] [expires: 2018-02-23]
sig!         290E99FF 2014-02-23  Bruno Kirschner <bruno.kirschner@online.de>

