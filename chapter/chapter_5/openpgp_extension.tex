%
% Beuth Hochschule für Technik --  Abschlussarbeit
%
% OpenPGP-Erweiterung
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[hbt]
   \centerline{\includegraphics[keepaspectratio, width=0.75\textwidth]{Blind_Signaturpacket.pdf}}
   \mycaption{Minimalinvasive Anpassung des OpenPGP-Signaturpackets}{Die Darstellung zeigt eine angepasste Version der Darstellung \ref{fig:Paket_Signatur} in der am Punkt (e) ein Unterpaket für den Algorithmus zur Verifizierung der blinden Signatur eingefügt wurde. Darüber hinaus werden keine weiteren Änderungen am \gls{OpenPGP}-Signaturpaket vorgenommen.}
   \label{fig:Unterpaket_Blinde_Signatur}
\end{figure}

\section{Erweiterung des OpenPGP-Nachrichtenformats}
\label{sec:Konzeption - OpenPGPExtension}
Neben der Umsetzung eines Prototypen unter Verwendung blinder, auf elliptischen Kurven basierender, Signaturverfahren und deren im vorhergehenden Abschnitt durchgeführten Sicherheitsanalyse, ist die Integration solcher Verfahren in aktuelle Standards eines der eingangs definierten Ziele (vgl. Abschnitt \ref{sec:Motivation - Zielsetzung} \nameref{sec:Motivation - Zielsetzung}). Dieses Ziel stellt die Basis für die beiden nicht-funktionalen Anforderungen \emph{NF.2 - Minimalinvasive Anpassung existierender Standards und Implementierungen} \& \emph{NF.3 - Kompatibilität zu existierenden Signaturen} dar. Im Rahmen ihre Realisierung beschreibt der folgende Abschnitt die Notwendigkeit einer Erweiterung des \gls{OpenPGP}-Nachrichtenformats und präsentiert anschließend ein entsprechendes Konzept.

\subsection{Analyse des Ist-Zustands}
Die Tatsache, dass, um die zuvor erwähnten Anforderungen zu erfüllen, eine Erweiterung des \gls{OpenPGP}-Nachrichtenformats notwendig ist, ist eine Folge der Betrachtungen der Abschnitte \ref{subsec:Stand der Technik - Struktur OpenPGP} (\nameref{subsec:Stand der Technik - Struktur OpenPGP}) und \ref{sec:Konzeption - Signaturverfahren} (\nameref{sec:Konzeption - Signaturverfahren}), in denen sich folgendes gezeigt hat:

\subsubsection{Signaturverfahren in OpenPGP}
Der aktuelle Stand des \gls{OpenPGP}-Formats enthält einige Signaturverfahren, so zum Beispiel das klassische \gls{RSA}-Signaturschema oder den im Rahmen des \gls{RFC} 6637 hinzugefügten \gls{ECDSA}, hierbei handelt es sich jedoch ausschließlich um konventionelle Verfahren, in denen die zu signierenden Daten dem Signierenden offen zur Verfügung stehen. Darüber hinaus konnten bei der Recherche zu dieser Arbeit keine Hinweise, egal ob positiver oder negativer Art, gefunden werden, die darauf hinweisen, dass es bereits aktive Bemühungen gegeben hat entsprechende Verfahren in den Standard zu integrieren.

\subsubsection{Kompatibilität blinder Signaturverfahren}
Obwohl es einige blinde Signaturverfahren gibt, deren resultierende Signaturen kompatibel zu bereits existierenden Signaturverfahren sind, ist dies im Bereich der auf elliptischen Kurven basierten Verfahren eine seltene Eigenschaft. Von den vier im Abschnitt \ref{sec:Konzeption - Signaturverfahren} analysierten Verfahren besitzt ausschliesslich \textcite{Andreev2015Blind} diese Eigenschaft, da es eine vollständig valide \gls{ECDSA}-Signatur erzeugt. Dies gilt jedoch nicht für die Signaturen der anderen Verfahren, so dass diese spezielle, nicht zueinander kompatible Verifizierungsalgorithmen benötigen. Da die einzelnen Signaturverfahren bisher nicht Bestandteil der Spezifikation von \gls{OpenPGP} sind, können die mit ihnen erzeugten Signaturen auch nicht mit standardkompatiblen Implementierungen wie \gls{GPG}\footnote{GPG ist eine weit verbreitete, offene Implementierung des OpenPGP-Standards. Der offizielle Auftritt des Projekts kann über \url{https://www.gnupg.org/} erreicht werden. (Zuletzt aufgerufen am 17. März 2016)} verifiziert werden.

Ein anderer Punkt, der in einem Erweiterungskonzept beachtet werden sollte, ist die Tatsache, dass die vorgestellten Verfahren zwar ihr eigenen Verifizierungsalgorithmus mitbringen, darüber hinaus jedoch stark an den \gls{ECDSA} angelehnt sind. So verwenden sie die selben Algorithmen zur Schlüsselerzeugung, was den Einsatz bereits existierender Schlüssel ermöglicht. Darüber hinaus ist auch der Aufbau des Signaturenformats stark an den Algorithmus angelehnt, wodurch jedes der hier behandelten Verfahren eine gleich kodierte Signatur erzeugt, so dass nur der eigentliche Inhalt der Signatur anders behandelt werden muss.

\subsection{Ansatz und Erläuterung notwendiger Anpassungen}
Mit dem Ziel die gesetzten Anforderungen \emph{NF.2 - Minimalinvasive Anpassung existierender Standards und Implementierungen} \& \emph{NF.3 - Kompatibilität zu existierenden Signaturen} zu Erfüllen, ergeben sich auf Basis der oben durchgeführten Analyse die folgenden zwei Ansätze, um das OpenPGP um die gewünschte Funktionalität zu erweitern.

\begin{table}[!t]
   \begin{minipage}{\textwidth}
      \begin{tabular}{c | l}
         \textbf{ID} & \textbf{Public-Key Algorithmus} \\
         \hline
         1 & RSA (verschlüsseln und signieren) \\
         2 & RSA (nur verschlüsseln) \\
         3 & RSA (nur signieren) \\
         16 & Elgamal (nur verschlüsseln) \\
         17 & DSA \\
         18 & ECDH*\\
         19 & ECDSA*\\
         20 & reserviert (früher Elgamal (verschlüsseln und signieren)) \\
         21 & reserviert für Diffie-Hellman (X9.42, IETF-S/MIME) \\
         100 - 110 & private /experimentelle Algorithmen
      \end{tabular}
      \footnotetext{* Im Rahmen des RFC 4880 reserviert und erst durch den RFC 6637 tatsächlich hinzugefügt. \autocite[S. 62]{RFC4880}}
   \end{minipage}
   \mycaption{Public-Key-Algorithmen des OpenPGP-Nachrichtenformats}{Die Übersicht zeigt die von OpenPGP unterstützten Public-Key-Algorithmen mit der ihnen im Standard zugeteilten Identifikationsnummer \autocite[S. 62]{RFC4880}. Hierbei wird ersichtlich, dass der Standard bereits Vorkehrungen für die Integration neuer Algorithmen getroffen sind.}
   \label{tab:OpenPGP - PublicKeyIDs}
\end{table}

%\vspace{2cm}
\enlargethispage{\baselineskip}
\subsubsection{Integration der Verfahren als eigenständige Algorithmen}
Da das OpenPGP-Nachrichtenformat bereits einige Signaturverfahren als eigenständige Public-Key-Algorithmen beinhaltet, wäre ein möglicher Ansatzpunkt, zur Erweiterung des Standards, die blinden Signaturverfahren ebenfalls als eigenständige Algorithmen zu integrieren. Hierfür müsste zunächst die, in Tabelle \ref{tab:OpenPGP - PublicKeyIDs} abgebildete, Liste der Public-Key-Algorithmen entsprechend ergänzt werden. Eine solche Ergänzung würde es ermöglichen eigene Schlüsselpaare und Pakete speziell für die neu eingefügten Algorithmen zu erzeugen, in denen, entsprechend den Festlegungen des Standards (siehe \autocite[S. 42]{RFC4880}), die Identifikationsnummer des Algorithmus enthalten wäre. Da die selbe Identifikationsnummer auch Bestandteil der Definition des Signaturpakets ist, wären die, mit diesen Schlüsseln erzeugten, Signaturen eindeutig von den klassischen Signaturen unterscheidbar.

Diese klare Unterscheidbarkeit, welche im Rahmen der klassischen Signaturverfahren gewünscht ist, ist jedoch in diesem Zusammenhang als künstlich eingeführter Nachteil zu betrachten. Denn, obwohl wie bereits angemerkt, alle betrachteten Signaturverfahren sowohl in der Schlüsselerzeugung, als auch im Aufbau der von ihnen erzeugten Signaturen existierenden Verfahren ähneln, wäre es so nicht möglich diesen Vorteil auszunutzen und bereits existierende Schlüssel zu verwenden. Stattdessen müsste der Signierende für jedes unterstützte Verfahren ein eigenes Schlüsselpaar erzeugen und dabei sowohl sicher stellen, dass der zugehörige private Schlüssel geheim gehalten wird, als auch, dass alle öffentlichen Schlüssel publiziert werden und es einem Verifizierendem möglich ist den für ihn relevanten öffentlichen Schlüssel zu erhalten.

\begin{table}[!b]
   \begin{minipage}{\textwidth}
      \begin{tabular}{c | l | c | l}
         \textbf{ID} & \textbf{Signaturunterpakettyp} & \textbf{ID} & \textbf{Signaturunterpakettyp} \\
         \hline
          0 & Reserved & 17 & Reserved \\
          1 & Reserved & 18 & Reserved \\
          2 & Signature Creation Time & 19 & Reserved \\
          3 & Signature Expiration Time & 20 & Notation Data \\
          4 & Exportable Certification & 21 & Preferred Hash Algorithms \\
          5 & Trust Signature & 22 & Preferred Compression Algorithms \\
          6 & Regular Expression & 23 & Key Server Preferences \\
          7 & Revocable & 24 & Preferred Key Server \\
          8 & Reserved & 25 & Primary User ID \\
          9 & Key Expiration Time & 26 & Policy URI \\
          0 & Placeholder for backward compatibility & 27 & Key Flags \\
          1 & Preferred Symmetric Algorithms & 28 & Signer's User ID \\
          2 & Revocation Key & 29 & Reason for Revocation \\
          3 & Reserved & 30 & Features \\
          4 & Reserved & 31 & Signature Target \\
          5 & Reserved & 32 & Embedded Signature \\
          6 & Issuer & 100 To 110 & Private or experimental \\
      \end{tabular}
   \end{minipage}
   \mycaption{Signaturunterpakete des OpenPGP-Nachrichtenformats}{Die Übersicht zeigt die von OpenPGP unterstützten Signaturunterpakete, wie sie im RFC 4880 im Unterabschnitt 5.2.3.1 spezifiziert werden \autocite[S. 25f]{RFC4880}.}
   \label{tab:OpenPGP - SignatureSubPacketIds}
\end{table}

\enlargethispage{-2\baselineskip}
\subsubsection{Erweiterung des Signaturpakets}
Eine alternative Herangehensweise zur Integration der blinden Signaturverfahren, ist die Erweiterung der Definition des Signaturpakets, im speziellen die Einführung eines neuen Signaturunterpakets. Statt wie im vorhergehenden Ansatz die neuen Verfahren als eigenständige Public-Key-Algorithmen in das OpenPGP-Nachrichtenformat zu integrieren, wird bei dieser Variante auf die vorhandene Ähnlichkeiten zu bereits existierenden Algorithmen, wie dem \gls{ECDSA}, aufgebaut. So soll es dem Signierenden ermöglicht werden, bereits existierende Schlüsselpaare für die Erzeugung blinder Signaturen zu verwenden, wodurch gleichzeitig vermieden wird, dass bei der Einführung neuer Verfahren auch neue Schlüsselpaare generiert werden müssen.

Die zu Grunde liegende Idee ist es hierbei, eine Möglichkeit in den Standard zu integrieren, die es erlaubt, im Rahmen der Signaturverifizierung, einen verfahrenseigenen Verifizierungsalgorithmus zu verwenden. Hierdurch kann eine Unterscheidung ermöglicht werden, zwischen dem blinden Signaturverfahren und dem Public-Key-Verfahren, welches verwendet wurde, um den öffentlichen Schlüssel  zu erzeugen. Dabei wird, analog zu bereits existierenden Unterpaketen (vgl. Tabelle \ref{tab:OpenPGP - SignatureSubPacketIds}) und entsprechend der Abbildung \ref{fig:Unterpaket_Blinde_Signatur}, ein neues Unterpaket \emph{Signature Verification Algorithm} etabliert, welches die Identifikationsnummer des verwendeten Signaturverfahrens enthält. Darüber hinaus wird das Paket, entsprechend der Spezifikation in \autocite[S. 26]{RFC4880}, durch das setzen des 7. Bit, als kritisch markiert, so dass eine Interpretation des Inhalts, im Rahmen der Überprüfung, verpflichtend wird. Kann eine ältere oder unvollständige Implementierung des Standards das neu integrierte Signaturunterpaket oder die darin spezifizierte Identifikationsnummer nicht verarbeiten, erhält der Überprüfende eine entsprechende Fehlermeldung.

Gleichzeitig ist es auch möglich, dass neu eingefügte Paket \emph{Signature Verification Algorithm} als einen Indikator für den Einsatz blinder Signaturverfahren zu etablieren, welche, wie das Verfahren von \textcite{Andreev2015Blind}, voll kompatibel zu bereits existierenden Public-Key-Algorithmen sind. Dies kann unter anderem durch die folgenden beiden Ansätze erreicht werden:

\begin{itemize}
   \item Eine Möglichkeit wäre es, die schon vorhandenen klassischen Signaturverfahren in die Liste der Verifizierungsalgorithmen aufzunehmen. Dies würde jedoch auf Grund der doppelten Zuordnung von Identifikationsnummer und Verfahren (siehe Tabelle \ref{tab:OpenPGP - PublicKeyIDs}) zu Uneindeutigkeiten führen. 
   
   \item Ein alternativer Ansatz ist die Festlegung einer als \glqq Standard\grqq{} bezeichneten Identifikationsnummer. Wird ein Unterpaket verwendet, welches mit dieser Nummer versehen ist, bedeutet dies für die jeweilige Implementierung, dass zur Verifizierung des Pakets der zum Public-Key-Algorithmus gehörige Verifizierungsalgorithmus zu verwenden ist.
\end{itemize}

Da im Rahmen des zweiten Ansatzes weniger Identifikationsnummern zu vergeben sind und kein Public-Key-Verfahren mit einer zweiten ID versehen wird, ist dieser Ansatz zu bevorzugen.