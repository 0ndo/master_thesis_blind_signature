%
% Beuth Hochschule für Technik --  Abschlussarbeit
%
% Anforderungen
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Anforderungsanalyse}
Für eine zielgerichtete Anforderungsanalyse wird, neben der Zielsetzungen aus Abschnitt \ref{sec:Motivation - Zielsetzung}, ein geeignetes Anwendungsszenario benötigt, welches anschließend, unter Beachtung der gesetzten Ziele, analysiert werden kann. Die Auswahl des Szenarios erfolgt in diesem Abschnitt. Die daraus resultierenden funktionalen und nicht-funktionalen Anforderungen werden entsprechend ihres Einflusses bewertet und bilden anschließend die Grundlage für die eigentliche Konzeption des Prototyps.

\begin{figure}[tb]
   \centerline{\includegraphics[keepaspectratio, width=0.75\textwidth]{Blind_Signature_Broker_Leak.pdf}}
   \mycaption{Schematische Darstellung eines blinden Signaturverfahren mit Makler (1)}{Dieses, im Vergleich zur Abbildung \ref{fig:Signaturschema}, erweiterte Verfahren verwendet einen Makler zur Vergabe von Pseudonymen. (1) Der Anfragende autorisiert sich, unter Verwendung seines Berechtigungsnachweises, gegenüber dem Makler. (2) Er erhält ein gültiges Pseudonym, welches ihn wiederum zum Erhalt von Signaturen berechtigt. (3) Nun kann unter Verwendung des Pseudonyms eine blinde Signatur angefordert werden. (4) Ob hierfür die notwendige Berechtigung vorliegt, wird vom Signierenden durch eine Überprüfung, des vom Makler ausgestellten Pseudonyms, festgestellt. (5) Signalisiert der Makler, dass für das gegebene Pseudonym die Teilnahmeberechtigung vorliegt, (6) kann die blinde Signatur erzeugt und zurück an den Signaturanfragenden übermittelt werden.}
   \label{fig:Signaturschema_Makler_Leak}
\end{figure}

\subsection{Auswahl eines geeigneten Anwendungsbereichs}
\label{subsec:Konzeption - Anwendungsbereich}
Die Auswahl eines geeigneten Anwendungsbereichs, für den Einsatz blinder Signaturen, stellt das Fundament der Anforderungsanalyse des zu konzipierenden Prototyps dar. Bekannte Anwendungsbereiche sind, entsprechend den Ausführungen von \textcites{Chaum1985Security}{Schneier1996Applied} unter anderem der Einsatz in anonymen elektronischen Wahlen und digitalen Währungen. Beiden Anwendungsfällen ist gemein, dass die im Rahmen des Verfahrens verwendeten blinden Signaturen nicht nur der Anonymisierung der zu signierenden Daten, also der elektronische Wählerstimmen oder digitale Münzen, verwendet werden. Sie dienen auch der Wahrung der Anonymität des Signaturanfragenden.

Dies wird, entsprechend der Abbildung \ref{fig:Signaturschema_Makler_Leak}, erreicht, indem eine dritte, vertrauenswürdige Partei in den Vorgang mit einbezogen wird. Sie stellt dem Anfragenden, nach ausgewiesener Berechtigung, eine als Pseudonym bezeichnete anonyme Identität aus (Schritt 1 und 2). Eine solche zusätzliche Partei erlaubt es, notwendige Berechtigungsüberprüfungen aus dem eigentlichen Vorgang der Signaturerzeugung auszulagern. Hierbei könnte es sich zum Beispiel um die Überprüfung handeln, ob eine Person an einer Wahl teilnehmen darf. Aufgrund ihrer Funktion als Mittelsmann wird die neu eingefügte Partei auch als \emph{Makler} bzw. \glqq Broker\grqq{} bezeichnet. Welche Art von Berechtigungsnachweis vom Makler gefordert wird, hängt von den Zugangsbedingungen des eigentlichen Verfahrens ab. Es könnte sich hierbei um eine Kopie des Personalausweises oder um ein geheimes Passwort handeln. Der Anfragende verwendet ab sofort ein vom Makler ausgestelltes Pseudonym, sodass er dem Signierenden seine wahre Identität nicht mitteilen muss. Im Rahmen einer Signaturanfrage wird der Makler vom Signierenden konsultiert (Schritt 4), um so die notwendige Auskunft über eine bestehende Berechtigung zu erhalten, ehe die Signatur erzeugt wird (Schritt 5) \autocite[S. 3]{Engel2012Pseudonyme}.

Ein Nachteil des beschriebenen Aufbaus ist es, dass der Makler, basierend auf den stetigen Anfragen des Signierenden, Informationen darüber erhält, welche Pseudonyminhaber Signaturanfragen stellen. Er erfährt also zum Beispiel, welche Personen an einer Wahl teilnehmen oder wie oft und wann digitales Geld in ein Konto eingezahlt oder abgehoben wird. Dies kann jedoch vermieden werden, indem der Makler die ausgegebenen Pseudonyme mit seinem privaten Schlüssel signiert. Das Pseudonym ist nun solange gültig wie der zur Signierung verwendete private Schlüssel. Anschließend ist es dem Signierende möglich, das öffentliche Gegenstück zu verwenden, um das Pseudonym eines Anfragenden auf seine Berechtigung zu überprüfen. So wird die Anonymität des Anfragenden gewahrt und es werden keine unnötigen Informationen an den Makler übermittelt \autocite[S. 51f]{Engel2012Pseudonyme}. Eine entsprechende Anpassung des Verfahrens ist in Abbildung \ref{fig:Signaturschema_Makler} dargestellt.

Um zu vermeiden, dass es dem Makler zu einem späteren Zeitpunkt möglich ist eine genaue Zuordnung zwischen einem Signaturanfragenden und einem Pseudonym herzustellen, sollte der Prozess der Pseudonymvergabe ebenfalls unter Verwendung blinder Signaturen durchgeführt werden. Hierbei wird der Makler selbst zum Signaturerzeugenden. Die Pseudonymvergabe folgt dabei dem in Abbildung \ref{fig:Signaturschema} dargestellten Verlauf, wobei die zu signierenden Daten dem gewünschten Pseudonym entsprechen, welches dem Makler somit unbekannt ist. Dies führt dazu, dass der Prozess aus Abbildung \ref{fig:Signaturschema_Makler} nun unter der Verwendung zweier blinder Signaturen statt findet.

\enlargethispage{-\baselineskip}
Diese erweiterte Form der Pseudonymerzeugung stellt somit einen eigenen Anwendungsbereich dar und kann in die beiden klassischen Einsatzbereichen blinder Signaturen integriert werden. Da es sich hierbei um den kleinsten gemeinsamen Nenner der drei beschriebenen Anwendungsbereiche handelt und um die Komplexität des  Systems zu reduzieren, wird die Konzeption des Prototyps auf der Grundlagen der Pseudonymvergabe mittels blinder Signaturen stattfinden. Sie bezieht sich auf den in Abbildung \ref{fig:Signaturschema} dargestellten Prozess und wird im Folgenden formalisiert. Hierfür werden im weiteren Verlauf die Rollen des Identitätenmaklers und die des Signierenden zusammengelegt. Der Signaturerzeugende aus den Abbildungen \ref{fig:Signaturschema_Makler_Leak} und \ref{fig:Signaturschema_Makler} entfällt und wird durch einen beliebigen Pseudonymüberprüfer ersetzt.

\begin{figure}[!t]
   \centerline{\includegraphics[keepaspectratio, width=0.9\textwidth]{Blind_Signature_Broker.pdf}}
   \mycaption{Schematische Darstellung eines blinden Signaturverfahren mit Makler (2)}{Das abgebildete Schema ist eine Erweiterung des Verfahrens in Abbildung \ref{fig:Signaturschema_Makler_Leak} mit der vermieden werden soll, dass der Makler über die Schritte zur Berechtigungsüberprüfung zu viele Informationen über den Anfragenden erhält. Hierfür wurde das System, um die Verwendung einer weiteren blinden Signatur des Pseudonyms, erweitert, wobei der zur Überprüfung benötigte Schlüssel in Schritt (3) veröffentlicht wird. Erhält der Signierende nun in Schritt (4) eine Signaturanfrage, kann er in (5) die Überprüfung des verwendeten Pseudonyms selbst übernehmen ohne erneut den Makler zu kontaktieren.}
   \label{fig:Signaturschema_Makler}
\end{figure}

\enlargethispage{\baselineskip}

\subsection{Formalisierung der Anwendungsfälle}
\label{subsec:Konzeption - Anwendungsfälle}
Basierend auf der vorangegangen Problembeschreibung der Pseudonymvergabe mittels eines Identiätenmaklers und unter Verwendung blinder Signaturen sowie der Darstellung des Verfahrens in Abbildung \ref{fig:Signaturschema_Makler}, lassen sich drei unabhängige Akteure und sechs Anwendungsfälle identifizieren. Diese sind in Abbildung \ref{fig:Use_Case} dargestellt und werden im Verlauf des Abschnittes formalisiert. Hierfür werden die Akteure und ihre Rollen innerhalb des Prozesses betrachten und die ihnen zugeordneten Anwendungsfälle spezifiziert.

\begin{figure}[!b]
   \centerline{\includegraphics[keepaspectratio, width=0.95\textwidth]{Use_Case.pdf}}
   \mycaption{Anwendungsfalldiagramme der blinden Erzeugung von Pseudonymen}{Das Diagramm bietet eine Übersicht über die beiden beteiligten Systeme und die notwendigen Interaktionsmöglichkeiten für die drei beteiligten Aktoren.}
   \label{fig:Use_Case}
\end{figure}

\subsubsection{Akteure}
Im Rahmen des in Abbildung \ref{fig:Use_Case} dargestellten Überblicks treten, äquivalent zu den Abbildungen \ref{fig:Signaturschema_Makler_Leak} und \ref{fig:Signaturschema_Makler}, drei unabhängige Akteure auf, deren Intentionen und Beziehungen untereinander nachfolgend beschrieben werden:

\paragraph{Der Signaturanfragende} Der Signaturanfragende möchte in den Besitz eines Pseudonyms gelangen, um so anonym an einem anderen Vorgang, zum Beispiel einer elektronischen Wahl, teilnehmen zu können. Für ihn ist die Pseudonymerstellung und damit der Kontakt mit dem Makler nur eine Notwendigkeit, da er an diesem anderen Vorgang teilnehmen möchte, in dessen Verlauf das Pseudonym überprüft wird. Er besitzt die grundlegenden Kenntnisse in der Verwendung kryptographischer Software und kann sich selbst eine digitale Identität in Form eines Schlüsselpaars erzeugen, welche ihm anschließend vom Makler signiert werden soll.

\paragraph{Der (automatisierte) Identitätenmakler} Der (automatisierte) Makler dient als Quelle für vertrauenswürdige Pseudonyme. Er ist ein Mittelsmann zwischen dem Anfragendem, welcher ein Pseudonym benötigt und einem etwaigen Pseudonymüberprüfer. Hierbei wird das Pseudonym verwendet, um eine anonyme und trotzdem vertrauenswürdige Kommunikation zwischen den beiden Parteien zu ermöglichen. Wird der Makler mit der Signierung eines Pseudonyms beauftragt, überprüft er zuerst die Berechtigung des Anfragenden und stellt das gewünschte Pseudonym nur nach einer erfolgreichen Authentifizierung aus. Die Art des Berechtigungsnachweises und der Überprüfung kann dabei je nach Anwendungsfall variieren.

\paragraph{Der Pseudonymüberprüfer} Ein Pseudonymüberprüfer ist eine beliebige Person (oder ein automatisierter Prozess), der die Gültigkeit eines Pseudonyms überprüft, um so festzustellen, ob es tatsächlich vom ausgewiesenen Makler vergeben wurde. So eine Überprüfung ist zum Beispiel notwendig, wenn festgestellt werden soll, ob ein Pseudonym an einem bestimmten Vorgang, zum Beispiel einer Wahl, teilnehmen darf oder nicht. Dies ist notwendig, da nur durch die Verwendung von Pseudonymen den Teilnehmer des Vorgangs, ein hohes Maß an Anonymität gewährleistet werden kann. Gleichzeitig wird so sichergestellt, dass es sich ausschließlich befugte Personen am Prozess teilnehmen. Der Makler ist für ihn eine vertrauenswürdige dritte Partei, der er die Aufgabe der Authentifizierung überträgt.

\subsubsection{Anwendungfälle}
Es folgt eine formalisierte Auflistung der in Abbildung \ref{fig:Use_Case} dargestellten Anwendungsfälle, welche die Interaktion der einzelnen Akteure mit dem System abbildet, ohne dabei auf die technischen Details einzugehen. Die darin enthaltenen Informationen ergeben sich direkt aus der Problembeschreibung im vorhergehenden Abschnitt \ref{subsec:Konzeption - Anwendungsbereich} und den oben stehenden Rollenbeschreibungen. Ein einzelner Anwendungsfall wird hierbei neben dem Namen, auch mit einer kurzen Beschreibung, den notwendigen Vorbedingungen, dem Primärszenario, den daraus resultierenden Nachbedingungen sowie eventuelle Sekundärszenarien beschrieben. Darüber hinaus erhält jeder eine Bezeichnung der Form \mbox{\glqq A. [Nr.]\grqq}, so dass eine genaue Referenzierung möglich ist.

Alle Anwendungsfälle zusammen bilden den Prozess der Erzeugung anonymer Pseudonyme mit Berechtigungsüberprüfung ab. Werden sie erfolgreich hintereinander ausgeführt, erhält der Pseudonymanfragende ein gültiges signiertes Pseudonym, welches von ihm selbst oder einer beliebigen dritten Person überprüft werden kann. Hierbei ist dem Makler das letztlich vergebene Pseudonym nicht bekannt.

\bigskip

\begin{enumerate}[label=\textbf{A. \arabic*}]
   \itemsep15pt
%
% A.1
%
   \item \textbf{Übertragung des Berechtigungsnachweises}
      \begin{description}[align=left, font=\itshape, style=nextline]
         \item[Kurzbeschreibung:] Der Pseudonymanfragende weist sich entsprechend den Anforderungen gegenüber dem Identitätenmakler aus.
         \item[Vorbedingung:] -
         \item[Nachbedingung:] Der Identitätenmakler hat den Berechtigungsnachweis des Anfragenden erhalten und kann ihm dem Anfragenden zuordnen.
         \item[Primärszenario:]
            \begin{enumerate}[label=(\arabic*)]
               \item[]
               \item Der Berechtigungsnachweis wird in das System eingegeben.
               \item Der Berechtigungsnachweis wird an den Makler übermittelt.
               \item Der Makler erhält den Berechtigungsnachweis und ordnet ihm dem Anfragenden zu.
            \end{enumerate}
         \item[Sekundärszenarien:]
            \begin{description}[align=left, font=\normalfont\itshape, style=nextline]
               \item[]
               \item[Übermittlung schlägt fehl] Sollte in Schritt 2 ein Fehler auftreten, erhält der Anfragende eine Fehlermeldung.
            \end{description}
      \end{description}
%
% A.2
%
   \item \textbf{Eingabe des gewünschten Pseudonyms}
      \begin{description}[align=left, font=\itshape, style=nextline]
         \item[Kurzbeschreibung:] Der Pseudonymanfragende hinterlegt in dem System des Maklers, das von ihm für die Signatur vorbereitete Pseudonym.
         \item[Vorbedingung:] Der Anfragende hat ein Pseudonym vorbereitet.
         \item[Nachbedingung:] Der Makler hat das gewünschte Pseudonym erhalten und dem Anfragenden zugeordnet.
         \item[Primärszenario:]
            \begin{enumerate}[label=(\arabic*)]
               \item[]
               \item Das Pseudonym wird in das System eingegeben.
               \item Das Pseudonym wird an den Makler übermittelt.
               \item Der Makler erhält das ausgeblendete Pseudonym und ordnet es dem Anfragenden zu.
            \end{enumerate}
         \item[Sekundärszenarien:]
            \begin{description}[align=left, font=\normalfont\itshape, style=nextline]
               \item[]
               \item[Übermittlung schlägt fehl] Sollte in Schritt 2 ein Fehler auftreten, erhält der Anfragende eine Fehlermeldung.
            \end{description}
      \end{description}
%
% A.3
%
   \item \textbf{Überprüfung der Berechtigung}
      \begin{description}[align=left, font=\itshape, style=nextline]
         \item[Kurzbeschreibung:] Der Identitätenmakler überprüft den Berechtigungsnachweis des Pseudonymanfragenden.
         \item[Vorbedingung:] Der Makler hat im Rahmen von A. 1 einen Berechtigungsnachweis eines Anfragenden erhalten.
         \item[Nachbedingung:] Der Berechtigungsnachweis wurde erfolgreich überprüft.
         \item[Primärszenario:]
            \begin{enumerate}[label=(\arabic*)]
               \item[]
               \item Der Makler entnimmt den erhaltenen Berechtigungsnachweis.
               \item Der Makler überprüft den Berechtigungsnachweis.
            \end{enumerate}
         \item[Sekundärszenarien:]
            \begin{description}[align=left, font=\normalfont\itshape, style=nextline]
               \item[]
               \item[Berechtigungsnachweis ungültig] Sollte in Schritt 2 festgestellt werden, dass der erhaltene Berechtigungsausweis ungültig ist, wird dies im System hinterlegt.
            \end{description}
      \end{description}
%
% A.4
%
   \item \textbf{Signieren eines Pseudonyms}
      \begin{description}[align=left, font=\itshape, style=nextline]
         \item[Kurzbeschreibung:] Der Makler signiert das Pseudonym und bestätigt die Identität.
         \item[Vorbedingung:] Der Makler hat im Verlauf von A.1 und A.2 vom Pseudonymanfragenden einen Berechtigungsnachweis und das gewünschte Pseudonym erhalten.
         \item[Nachbedingung:] Das vom Anfragenden erhaltene Pseudonym ist gültig signiert.
         \item[Primärszenario:]
            \begin{enumerate}[label=(\arabic*)]
               \item[]
               \item Der Makler entnimmt dem System das zu signierende Pseudonym und den Berechtigungsnachweis.
               \item Der Berechtigungsnachweis wird entsprechend A.3 überprüft.
               \item Das Pseudonym wird entsprechend signiert und damit für gültig erklärt.
            \end{enumerate}   
         \item[Sekundärszenarien:]
            \begin{description}[align=left, font=\normalfont\itshape, style=nextline]
               \item[]
               \item[Pseudonym ungültig] Ist das in Schritt 1 entnommene Pseudonym ungültig, wird eine Fehlermeldung an den Anfragenden übertragen.
               \item[Berechtigungsnachweis ungültig] Wird in Schritt 2 festgestellt, dass der Berechtigungsnachweis nicht ausreichen oder ungültig ist, erhält der Anfragende eine Fehlermeldung.
            \end{description}
      \end{description}
%
% A.5
%
   \item \textbf{Erhalt eines signierten Pseudonyms}
      \begin{description}[align=left, font=\itshape, style=nextline]
         \item[Kurzbeschreibung:] Der Pseudonymanfragende erhält das signierte Pseudonym vom Identitätenmakler.
         \item[Vorbedingung:] Das gewünschte Pseudonym und der Berechtigungsnachweis wurden erfolgreich beim Makler eingereicht.
         \item[Nachbedingung:] Der Anfragende hat ein signiertes Pseudonym erhalten.
         \item[Primärszenario:]
            \begin{enumerate}[label=(\arabic*)]
               \item[]
               \item Der Anfragende fordert das signierte Pseudonym beim Makler an.
               \item Der Makler überprüft ob A.3 und A. 4 erfolgreich ausgeführt wurden.
               \item Der Makler entnimmt dem System das von ihm signierte Pseudonym.
               \item Der Makler überträgt das signierte Pseudonym an den Anfragenden.
               \item Der Anfragende erhält das signierte Pseudonym.
            \end{enumerate}
         \item[Sekundärszenarien:]
            \begin{description}[align=left, font=\normalfont\itshape, style=nextline]
               \item[]
               \item[Überprüfung fehlgeschlagen] Sollte der Vorgang A. 3 oder A. 4 fehlgeschlagen sein, überträgt der Makler eine Fehlermeldung an den Anfragenden.
               \item[Übertragung schlägt fehlt] Schlägt die Übertragung zwischen Makler und Anfragendem in Schritt 4 fehlt erhält der Anfragende eine Fehlermeldung.
            \end{description}
      \end{description}
%
% A.6
%
   \item \textbf{Validierung eines signierten Pseudonyms}
      \begin{description}[align=left, font=\itshape, style=nextline]
         \item[Kurzbeschreibung:] Ein Pseudonymanfragender oder Pseudonymüberprüfer validiert die Gültigkeit eines erhaltenen signierten Pseudonyms in Bezug auf einen vom Makler veröffentlichten Schlüssel.
         \item[Vorbedingung:] Ein signiertes Pseudonym, der veröffentlichte Schlüssel zur Überprüfung und eine auf die Überprüfung des verwendeten Signaturverfahrens angepasste Software liegen vor.    
         \item[Nachbedingung:] Es wurde festgestellt, ob das Pseudonym mit dem vorliegenden Schlüssel signiert wurde.         
         \item[Primärszenario:]
            \begin{enumerate}[label=(\arabic*)]
               \item[]
               \item Der Überprüfende lädt den öffentlichen Schlüssel die Software.
               \item Der Überprüfende lädt das Pseudonym in die Software.
               \item Der Überprüfende weist die Software an eine Validierung der Signatur des vorliegenden Pseudonyms vorzunehmen.
               \item Die Software bestätigt, dass es sich um ein gültiges Pseudonym handelt.
            \end{enumerate}
         \item[Sekundärszenarien:]
            \begin{description}[align=left, font=\normalfont\itshape, style=nextline]
               \item[]
               \item[Schlüssel ungültig] Wird in Schritt 1 festgestellt, dass es sich um einen fehlerhaften Schlüssel handelt, bricht der Vorgang ab und der Überprüfende erhält eine Fehlermeldung.
               \item[Pseudonym fehlerhaft] Wird in Schritt 2 festgestellt, dass es sich um ein fehlerhaftes Pseudonym handelt, bricht der Vorgang ab und der Überprüfende erhält eine Fehlermeldung.
               \item[Signatur ungültig] Wird in Schritt 4 festgestellt, dass die Signatur fehlerhaft ist, erhält der Überprüfende eine entsprechende Fehlermeldung.
            \end{description}
      \end{description}
\end{enumerate}

\subsection{Funktionale Anforderungen}
\label{subsec:Konzeption - FA}
Basierend auf den im vorhergehenden Abschnitt aufgelisteten Anwendungsfällen und den Eingangs definierten Zielen, lässt sich eine Liste funktionaler Anforderungen zusammenstellen, welche der gewählte Anwendungsbereich an den zu entwickelnden Prototypen stellt. Sie werden in diesem Unterabschnitt in Kürze zusammengefasst und mit einer eindeutigen Bezeichnung der Form \mbox{\glqq F. [Nr.]\grqq{}} sowie mit einer Angabe des entsprechenden Anwendungsfalls versehen. Hierdurch wird im weiteren Verlauf eine genaue Zuordnung ermöglicht.

\begin{description}[align=left, style=nextline]
   \item[Der Pseudonymanfragende ...]

   \begin{enumerate}[label=\textbf{F. \arabic*}, align=left]
      \item[]
      \item ... kann den Berechtigungsnachweis in die Benutzeroberfläche eingeben. (A. 1)
      \item ... kann das Pseudonym in die Benutzeroberfläche eingeben. (A. 2)
      \item ... kann die eingegebenen Informationen an den Makler übermitteln. (A. 1, 2)
      \item ... bekommt eine Fehlermeldung im Falle eines Fehlers. (A. 1, 2, 5)
      \item ... bekommt im Erfolgsfall eine Ausgabe des signierten Pseudonyms. (A. 5)
      \item ... bekommt die notwendigen Informationen, um das signierte Pseudonym zu validieren. (A. 6)
   \end{enumerate}

   \item[Der Identitätenmakler ... ]
   \begin{enumerate}[label=\textbf{F. \arabic*}, align=left, resume]
      \item[]
      \item ... bekommt zur Anfragenvalidierung den Berechtigungsnachweis übermittelt. (A. 3)
      \item ... bekommt das gewünschte Pseudonym übermittelt, um es zu signieren. (A. 4)
      \item ... kann den Berechtigungsnachweis überprüfen. (A. 3)
      \item ... bricht den Vorgang ab, falls der Berechtigungsnachweis ungültig ist. (A. 4)
      \item ... kann das erhaltene Pseudonym signieren. (A. 4)
      \item ... kann das signierte Pseudonym an den Anfragenden übermitteln. (A. 5)
      \item ... kann im Fehlerfall eine Meldung an den Anfragenden senden. (A. 4, 5)
      \item ... veröffentlicht alle notwendigen Informationen, um die Signatur zu überprüfen. (A. 6)
   \end{enumerate}

   \item[Der Pseudonymüberprüfende ... ]

   \begin{enumerate}[label=\textbf{F. \arabic*}, align=left, resume]
      \item[]
      \item ... kann die Signatur eines Pseudonyms genauso behandeln wie jede andere. (A. 6)
      \item ... kann die Überprüfung der Signatur mit einer standardkonformen Implementierung vornehmen. (A. 6)
   \end{enumerate}
\end{description}

\subsection{Nicht-funktionale Anforderungen}
Neben den aus den zuvor beschriebenen funktionalen Anforderungen, welche sich direkt aus den Anwendungsfällen definierten, ergibt sich auch eine Reihe an nicht-funktionalen Anforderungen. Diese basieren hauptsächlich auf der Eingangs im Abschnitt \ref{sec:Motivation - Zielsetzung} definierten Zielsetzung und helfen den einen Rahmen für die anschließende Konzeption und die spätere Umsetzung aufzustellen. Sie werden in diesem Unterabschnitt in Kürze zusammengefasst und mit einer eindeutigen Bezeichnung der Form \mbox{\glqq NF. [Nr.]\grqq{}} versehen, um im weiteren Verlauf eine genaue Zuordnung zu ermöglichen.

\begin{enumerate}[label=\indent{\textbf{NF. \arabic*}}, align=left]
   \item \textbf{Verwendung des Prototyps mit minimalen Vorkenntnissen} \\
   Die Einstiegshürde für die Verwendung des Prototyps sollte möglichst gering gehalten werden, um so aufzuzeigen, dass der Einsatz blinder Signaturen auch in Bereichen möglich ist, in denen beim Anwender nicht mit starkem technischem Vorwissen zu rechen ist.
   
   \item \textbf{Minimalinvasive Anpassungen existierender Standards und Implementierungen} \\
   Sollten Anpassungen an existierenden Standards durchgeführt werden, sollten diese so klein wie möglich sein, um so zu verhindern, dass eine Adaption der vorgeschlagenen Änderungen auf Grund ihrer Komplexität scheitert.

   \item \textbf{Kompatibilität zu existierenden Signaturen} \\
   Die Handhabung und Überprüfung blinder Signaturen darf sich für den Endnutzer nicht von der regulärer Signaturen unterscheiden, da es ansonsten zu Verwirrungen beim Endnutzer kommen kann und entsprechend größere Anpassungen an existierender Software nötig werden.
   
   \item \textbf{Vergleichsimplementierungen} \\
   Um zu überprüfen, dass die geplanten Anpassungen nicht nur zu einem speziellen Verfahren kompatibel sind, müssen mindestens zwei verschiedene Signaturverfahren sowie eine nicht auf elliptischen Kurven basierte Referenzimplementierung vorhanden sein.

   \item \textbf{Strukturiert und Nachvollziehbar} \\
   Um die Akzeptanz für eine kryptographische Implementierung zu erhöhen, muss der Quellcode öffentlich verfügbar gemacht werden und eine klare, leicht verständliche Struktur aufweisen. Gleichzeitig muss darauf geachtet werden, dass der verfügbare Quellcode lesbar und aussagekräftig ist.
\end{enumerate}

\subsection{Anforderungsbewertung}
\label{subsec:Konzeption - Anforderungsbewertung}
Da es sich bei den aus den Anwendungsfällen A. 1-6 ergebenen funktionalen Anforderungen F. 1-16 um die Repräsentation eines minimalistischen Prototypen handelt, sind sie alle als kritische Bestandteile des Projekts anzusehen und müssen für eine erfolgreichen Projektverlauf umgesetzt werden.

Im Gegensatz zu ihnen, sind die nicht-funktionalen Anforderungen NF. 1-5 eher als unkritische Richtlinien zu betrachten. Sie sollten im Verlauf der Implementierung des Prototyps beachtet werden, eine Nichtumsetzung wird jedoch nicht als Grund betrachtet das Projekt als gescheitert anzusehen. Eine herausgestellte Position nimmt dabei die Anforderung NF. 4 ein, da die durch ihre Umsetzung gewonnen Erkenntnisse einen wichtigen Ansatz für die finalen Bewertung Projekt darstellen werden.
