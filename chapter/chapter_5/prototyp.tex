%
% Beuth Hochschule für Technik --  Abschlussarbeit
%
% Prototyp Konzeption
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Systementwurf}
Basierend auf den im vorangegangenen Abschnitt spezifizierten Anforderungen, wird in diesem Abschnitt das grundlegende Konzept für einen Prototypen entworfen. Zu diesem Zweck werden in den folgenden Unterabschnitten zunächst die Anforderungen analysiert, um dann eine Übersicht über die direkten und indirekten Konsequenzen für den Entwurf des Prototyps zu erhalten. Diese werden anschließend verwendet um eine geeignete Plattform für die Umsetzung festzulegen und die Architektur des Prototyps zu entwerfen.

\subsection{Konsequenzen aus der Anforderungsanalyse}
Um Konsequenzen aus der vorangegangen Anforderungsanalyse zu ziehen, werden an dieser Stelle die in ihr definierten funktionalen und nicht-funktionalen Anforderungen erneut betrachtet. Dient dazu, um im Folgenden die Art der Anwendung und die Zielplattform, als Basis für die eigentliche Architektur des Prototyps, zu bestimmen.

\subsubsection{Bestimmung des zu Grunde liegenden Modells}
Da es sich, entsprechend der einführenden Beschreibung im Abschnitt \ref{subsec:Kryptographische Grundlagen - Signaturen} (\nameref{subsec:Kryptographische Grundlagen - Signaturen}), bei den blinden Signaturverfahren um verteilte Verfahren handelt, in deren Prozess mindestens zwei Parteien miteinander agieren, muss dies durch die Architektur des Prototyps wieder gegeben werden (vgl. auch \cref{fig:Signaturschema,fig:Signaturschema_Makler_Leak,fig:Signaturschema_Makler}).

Gleichzeitig bietet die Pseudonymvergabe, welche im vorhergehenden Abschnitt als Anwendungsfall bestimmt wurde und die Art der Interaktion zwischen Signaturanfragendem und Signierendem (hier der Makler) eine Beziehung ähnlicher eines Dienstleistungsangebots. Dies zeigt sich daran, dass die eigentliche Intention des Signaturanfragenden nicht die Kommunikation mit dem Makler ist. Vielmehr möcht er mit einer dritten Partei (hier Pseudonymüberprüfer) anonym in Kontakt treten. Zuvor muss er jedoch explizit die Dienstleistung des Maklers anfordern, um darauf hin ein valides Pseudonym zu seiner Authentifizierung vorlegen zu können. Eine solche Beziehung lässt sich durch die Verwendung des \emph{Client-Server-Modells} abbilden, welches die Verteilung dienstleistungsähnlichen Anwendungen in Netzwerken beschreibt. Der Server, welcher hier mit dem Signierenden gleichzusetzen ist, bietet die Vergabe von Pseudonymen an und der Signaturanfragende, dargestellt durch den Client, kann, solange das Angebot vorhanden ist, durch eine Anfrage an den Server ein Pseudonym anfordern. Dies bietet neben der notwendigen Abstraktion zweier unabhängiger Parteien auch den Vorteil, dass diese sich nicht auf dem selben System befinden müssen, so dass eine Interaktion auch auf Distanz ermöglicht wird.

\subsubsection{Bestimmung einer geeigneten Plattform}
Basierend auf dem Client-Server-Modell existieren eine Reihe möglicher Plattformen zur Umsetzung des Prototyps, welche sich, vor allem für den Signaturanfragenden, in Kriterien wie Anwenderfreundlichkeit, notwendige Vorkenntnisse als auch Verfügbarkeit und dem Vertrauen in die Sicherheit des Systems unterscheiden. So sind, je nach Gewichtung der Kriterien und dem Betrachtungsstandpunkt, unter anderem die Umsetzung als Desktop-Anwendung, Web-Applikation als auch die Realisierung einer mobilen Anwendung in Betracht zu ziehen. Zieht man für diese Entscheidung die nicht-funktionalen Anforderung hinzu, zeigt sich, dass vor allem die erste Anforderung (\emph{NF. 1}) und die in ihr geforderte hohe Verfügbarkeit durch eine geringe Einstiegshürde für Nutzer ohne weitere Vorkenntnisse als relevant betrachtet werden muss.

Da das Konzept einer Web-Applikation von allen angesprochenen Realisierungsmöglichkeiten die höchste Verfügbarkeit hat, da es als hybrides System sowohl auf Desktop-Systemen, als auch im mobilen Bereich Anwendung finden kann, bietet es einige Vorteile gegenüber den anderen Vorschlägen. Darüber hinaus ist davon auszugehen, dass die Verwendung eines Browsers und der Aufruf einer Internetseite heutzutage zu den notwendigen Grundkenntnissen in der Verwendung von Computern gehört, sodass die Einstiegshürde für den Anwender möglichst gering gehalten wird. Aus diesen Gründen wird der Prototyp im Folgenden als Webapplikation konzipiert.

\subsection{Entwurf einer Web-Applikation}
Im nächsten Schritt werden die Komponenten des Prototyps anhand der funktionalen Anforderungen \emph{F. 1-16} identifiziert und beschrieben. Dabei ist zu beachten, dass basierend auf der verteilten Struktur der blinden Signaturverfahren und durch die Entscheidung zur Entwicklung einer Web-Applikation im Client-Server-Modell, der Prototyp aus zwei Einzelsystemen besteht, welche im Folgenden nur noch als \glqq Client\grqq{} und \glqq Server\grqq{} bezeichnet werden. Hierbei stellt der Client das System des Signaturanfragenden dar und der Server das des Signierenden.

\subsubsection{Identifikation der funktionalen Komponenten}
Um die wichtigsten Komponenten der Systeme zu identifizieren, werden die funktionalen Anforderungen (vgl. Abschnitt \ref{subsec:Konzeption - FA} - \nameref{subsec:Konzeption - FA}) betrachtet und entsprechend der in ihnen abgebildeten Funktionen gruppiert:

\begin{description}[align=left, labelindent=1cm, leftmargin=1cm]
   \item[Signaturverfahren] Die wichtigsten Bestandteile des allgemeinen Ablaufs blinder Signaturverfahren sind in den Anforderungen F. 1-2, F. 4, F. 11, F. 14 abgebildet. Hierbei sind die Anforderungen zur Kommunikation zwischen den beiden Parteien herausgenommen.
   \item[Kommunikation] Die folgenden funktionalen Anforderungen behandeln, die zur Kommunikation zwischen Client und Server notwendigen, Bestandteile des Systems: F. 3, F. 5 - 8, F. 12-13.
   \item[Berechtigungsüberprüfung] Die Berechtigungsüberprüfung zur Authentifizierung des Clients wird in F. 9-10 beschrieben. Sie ist jedoch kein Bestandteil des eigentlichen Signaturverfahrens.
   \item[OpenPGP-Implementierung] Die Anforderungen zur Validierung des signierten Pseudonyms sind als Anforderungen an den zu erweiternden Standard zu sehen: F. 15-16
\end{description}

Wird diese Gruppierung gleichzeitig auf den jeweiligen Akteur projiziert, entsteht die in Abbildung \ref{fig:Systemkomponenten} enthaltene Übersicht. Hierbei zeigt sich, dass die Komponenten zum Signaturverfahren und zur OpenPGP-Implementierung in beiden Zielsystemen enthalten sind. Neu hinzukommt außerdem eine Komponente zur Erzeugung des finalen Signaturpakets, welche verwendet wird um das im Verlauf des Verfahrens erzeugte Signaturpaket in eine OpenPGP-konforme Form zu überführen. Es dient als Schnittstelle zwischen der OpenPGP-Implementierung und den verschiedenen Signaturverfahren. Nicht als eigenständige Komponente übernommen wurde dahingegen die Kommunikation. Ihre Funktion wird, wie in der Abbildung dargestellt, teilweise von der Interaktion zwischen Webbrowser und Server übernommen (a \& b) oder ist als Teil des Signaturverfahrens (c, d \& e) zu betrachten.

\subsubsection{Aufbau der Systeme}
Neben der bisherige Analyse der Komponenten als einzelne Elemente, ist auch eine Beschreibung der Gesamtsysteme notwendig. Hierfür wird im Folgenden das Zusammenspiel der Komponenten untereinander und ihr Aufbau aufeinander beschrieben.

Um das Ziel der Integration der Verfahren in das OpenPGP-Nachrichtenformat abbilden zu können, wird die Komponente \emph{OpenPGP-Implementierung} als Grundlage für beide Systeme betrachtet. In beiden Fällen dient diese Komponente hauptsächlich der Verifizierung des erzeugten Pseudonyms und der Überprüfung, ob die gegebenen Ziele zur Gleichbehandlung von Signaturen und blinden Signaturen umgesetzt wurde. Gleichzeitig wird so vermieden, dass elementare Standardfunktionen, wie das Einlesen von Schlüsseln und die mathematischen Operationen auf elliptischen Kurven neu implementiert werden muss. Hierbei ist anzumerken, dass es sich im Client- und im Serversystem nicht um die selbe Implementierung handeln muss, da die notwendigen Anpassungen zur Integration der Verifizierungsalgorithmen durch ihr minimalinvasives Design auch in mehr als einem Projekte erfolgen kann.

Im Client-System dient, wie im vorhergehenden Abschnitt bereits angemerkt, die \emph{Signaturpaketerzeugng} als Schnittstelle zwischen der zum Standard konformen OpenPGP-Implementierung und den neu eingeführten Signaturverfahren und hilft so die erzeugten Pseudonyme in valide OpenPGP-Signaturen zu überführen. Da dies auf Seiten des Servers nicht notwendig ist, existiert hier keine Schnittstellenkomponente. Hierfür enthält es die Komponente zur \emph{Berechtigungsüberprüfung}, um so die Authentifizierung der erhaltenen Anfrage vornehmen zu können.

In beiden Systemen sollte die \emph{Signaturverfahren}-Komponente die OpenPGP-Implementierung als Basis verwenden, um so die Kompatibilität zum OpenPGP-Standard sicherzustellen.

\begin{figure}[t]
   \centerline{\includegraphics[keepaspectratio, width=0.9\textwidth]{Systemkomponenten.pdf}}
   \mycaption{Client-Server-Kommunikation und Systemkomponenten}{Die Abbildung zeigt die 5 Schritte der Interaktion zwischen Client und Server, welche sich wie folgt aufgliedert. (a) Der initialen Aufruf der Internetseite, welcher den Vorgang anstößt und gleichzeitig zur Wahl des Signaturverfahrens dient. (b) Im direkten Anschluss wird das Clients-Skripts an den Nutzer übertragen. (c) Dies wird verwendet, um das gewünschten Pseudonyms und den notwendigen Berechtigungsnachweis zurück an den Server zu übermitteln. (d) Daraufhin erfolgt eine automatisierte Kommunikation im Rahmen des ausgewählten Verfahrens, an dessen Ende (e) das signierten Pseudonyms ausgehändigt wird.}
   \label{fig:Systemkomponenten}
   \vspace{-0.5\baselineskip}
\end{figure}